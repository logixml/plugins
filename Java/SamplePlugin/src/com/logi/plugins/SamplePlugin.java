package com.logi.plugins;

import java.util.Enumeration;


import com.logixml.plugins.LogiPluginObjects10;



public class SamplePlugin {
	/*
	 * 
	 * Simple conversion from C# code.
	 * This plugin adds an OPTION (RECOMPILE) to the end of the Active SQL generated query.
	 * 
	 */
	public void modifyActiveSql(LogiPluginObjects10 rdObjects) {
		String query = rdObjects.getSQLQuery() + "\nOPTION (RECOMPILE)";
		
		// Original Query
		rdObjects.addDebugMessage(" ", "ActiveSql Plugin", "Sql Query", rdObjects.getSQLQuery());
		
		// Reset the Query in the Engine
		rdObjects.setSQLQuery(query);
		
		// Modified Query
		rdObjects.addDebugMessage(" ", "ActiveSql Plugin", "Modified Query", rdObjects.getSQLQuery());
		
	
	}
	
	public void getGeneratedElementPluginCallParameters(LogiPluginObjects10 rdObjects) {
		String test1 = (String)rdObjects.getPluginParameters().get("test");
		String test2 = (String)rdObjects.getPluginParameters().get("test2");
		rdObjects.addDebugMessage("Plugin Parameters", "test", test1);
		rdObjects.addDebugMessage("Plugin Parameters", "test2", test2);
		
		String query = "Do Sleep(34);\n" + rdObjects.getSQLQuery();
		
		rdObjects.setSQLQuery(query);
		
		@SuppressWarnings("unchecked")
		Enumeration<String> params = rdObjects.getPluginParameters().keys();
		
		while(params.hasMoreElements()) {
			String param = params.nextElement();
			rdObjects.addDebugMessage("Plugin Parameters", " ", param, (String)rdObjects.getPluginParameters().get(param));
		}
		
				
	}
}
