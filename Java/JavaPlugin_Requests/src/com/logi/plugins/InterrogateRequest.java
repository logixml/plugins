package com.logi.plugins;

import java.util.Vector;

import com.logixml.plugins.LogiPluginObjects10;

public class InterrogateRequest {
	
	public void outputRequestToDebug(LogiPluginObjects10 rdObjects) {
		@SuppressWarnings("unchecked")
		Vector<String> names = rdObjects.getRequestParameterNames();
		@SuppressWarnings("unchecked")
		Vector<String> vals = rdObjects.getRequestParameterValues();
		rdObjects.addDebugMessage("Request Plugin", "Found", names.size() + " request variables");
		rdObjects.addDebugMessage("Request Plugin", "Found", vals.size() + " request variable values");
		for (int i = 0; i < names.size(); i++) {
			rdObjects.addDebugMessage("", names.get(i), vals.get(i));
		}
		
	}
}
