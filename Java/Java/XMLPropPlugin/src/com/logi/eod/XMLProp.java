package com.logi.eod;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import com.logixml.plugins.LogiPluginObjects10;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class XMLProp {

	public void addDashboardFriendlyName(LogiPluginObjects10 rdObjects) {
		String sName = (String) rdObjects.getPluginParameters().get("Name");
		String sValue = (String) rdObjects.getPluginParameters().get("Value");
		String filePath = (String) rdObjects.getPluginParameters().get("Path");
		
        
		File xmlFile = new File(filePath);
        
        rdObjects.addDebugMessage("2",xmlFile.getAbsolutePath());
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        	try {
        			rdObjects.addDebugMessage("","sName",sName);
		            dBuilder = dbFactory.newDocumentBuilder();
		            Document doc = dBuilder.parse(xmlFile);
		            doc.getDocumentElement().normalize();
		            
		            Element docEle = doc.getDocumentElement();
		            docEle.setAttribute(sName,sValue);
		            TransformerFactory transformerFactory = TransformerFactory.newInstance();
		            Transformer transformer = transformerFactory.newTransformer();
		            DOMSource source = new DOMSource(doc);
		            StreamResult result = new StreamResult(new FileOutputStream(filePath));
		            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		            transformer.transform(source, result);
		            
        } catch (SAXException | ParserConfigurationException | IOException | TransformerException e1) {
		            e1.printStackTrace();
		}
	}
}
