package com.logi.plugins;

import org.w3c.dom.*;
import org.w3c.dom.Document;
import com.logixml.plugins.LogiPluginObjects10;

import javax.xml.parsers.*;
import java.io.*;
import java.text.ParseException;
import javax.servlet.http.HttpSession;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import javax.xml.xpath.XPathFactory;
import org.w3c.dom.ls.LSException;
import org.xml.sax.SAXException;

public class PluginMethods {

	// ************************************************************************
	// This method sets the color attribute in an AnalysisChart's Series element
	// to the value of the selected Label Column's data value.
	// ************************************************************************
	public void setColorsFromSelectedColumnWithTranslation(LogiPluginObjects10 lgxObj) throws FileNotFoundException, ParseException, XPathExpressionException {

		lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Pre-Processing AnalysisChart Definition ...", lgxObj.getCurrentDefinition());

		try {
			// ...instantiate and Load the current report definition into an XmlDocument
			// object
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			byte b[] = lgxObj.getCurrentDefinition().getBytes("UTF8");
			java.io.ByteArrayInputStream iptStr = new java.io.ByteArrayInputStream(b);
			Document doc = dBuilder.parse(iptStr);

			XPath xpath = XPathFactory.newInstance().newXPath();

			// ...parse out the ChartCanvas Series XDataColumn or LabelColumn (for pie)
			NodeList chartSeriesNodeList = (NodeList) xpath.compile("//ChartCanvas/Series").evaluate(doc, XPathConstants.NODESET);
			if (chartSeriesNodeList != null && chartSeriesNodeList.getLength() > 0) {

				lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumn", "Chart Series Count = " + chartSeriesNodeList.getLength());

				for (int i = 0; i < chartSeriesNodeList.getLength(); i++) {
					Element seriesElement = (Element) chartSeriesNodeList.item(i);
					// Element seriesElementSqlCrosstab = (Element)
					// chartSeriesNodeListSqlCrosstab.item(i); - This does not guarantee we are
					// talking about the same charts.

					String sID = ((Element) seriesElement.getParentNode()).getAttribute("ID");
					lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Updating Chart with ID " + sID);

					NodeList chartSeriesNodeListSqlCrosstab = (NodeList) xpath.compile("//ChartCanvas[@ID='" + sID + "']/DataLayer/SqlCrosstab").evaluate(doc, XPathConstants.NODESET);

					String seriesXDataColumn = null;
					String seriesXDataColumnAttribute = null;
					String sCrosstabCustomColorColumn = null;

					if (chartSeriesNodeListSqlCrosstab != null && chartSeriesNodeListSqlCrosstab.getLength() > 0) {

						lgxObj.addDebugMessage("Plugin Call", "Chart Series SqlCrosstab", "Found");
						
						// uses SqlCrosstab//CrosstabColumn
						// for (int j = 0; j < chartSeriesNodeListSqlCrosstab.getLength(); j++) {
						seriesXDataColumnAttribute = "CrosstabColumn";

						seriesXDataColumn = ((Element) chartSeriesNodeListSqlCrosstab.item(0)).getAttribute(seriesXDataColumnAttribute);

						// Change the Crosstab Column used in the SqlCrosstab to the column we will make
						// later.
						// seriesElementSqlCrosstab.setAttribute(seriesXDataColumnAttribute,
						// "rdCustomColorColumn");

						// ...New Extra Crosstab Value Column for Crosstab Filter
						sCrosstabCustomColorColumn = "rdCustomCrosstabColorColumn";

					} else {
						// ...set the correct attribute name for selected label from the Chart Series
						// depending on chart type
						// ...bar, line, scatter, spline all use attribute named ChartXDataColumn
						seriesXDataColumnAttribute = "ChartXDataColumn";
						if (seriesElement.getAttribute("Type").equalsIgnoreCase("Pie")) {
							// ...pie uses attribute named ChartLabelColumn
							seriesXDataColumnAttribute = "ChartLabelColumn";
						}
						seriesXDataColumn = seriesElement.getAttribute(seriesXDataColumnAttribute);
					}

					// String colorDataColumn =
					// seriesXDataColumn.substring(seriesXDataColumn.lastIndexOf('_')+1);

					lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Chart SeriesXDataColumn: " + seriesXDataColumn);

					// ...if a ChartCanvas Series XDataColumn ends in "Color"
					// ...we use it for the color else we do nothing
					if (seriesXDataColumn.toUpperCase().endsWith("COLOR")) {

						// ***************************************************************************
						// ***************************************************************************
						// ...get the CC DataLayer node for processing
						// ...should only be 1 as the immediate child of the CC
						NodeList chartDataLayerNodeList = (NodeList) xpath.compile("//ChartCanvas[@ID='" + sID + "']/DataLayer").evaluate(doc, XPathConstants.NODESET);
						lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "chartDataLayerNodeList = " + chartDataLayerNodeList.getLength());
						if (chartDataLayerNodeList.getLength() > 0) {
							Node ccDataLayerNode = chartDataLayerNodeList.item(0);
							NodeList ccDataLayerChildNodeList = ccDataLayerNode.getChildNodes();

							lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "ccDataLayerChildNodeList = " + ccDataLayerChildNodeList.getLength());

							if (ccDataLayerChildNodeList.getLength() > 0) {
								// ...default to the first child node of the CC DataLayer
								Node dlChildNodeToInsertBefore = ccDataLayerChildNodeList.item(0);

								// ...find SqlGroup element so we can add a new SqlCalculatedColumn
								// ...element immediately before the SqlGroup element
								for (int j = 0; j < ccDataLayerNode.getChildNodes().getLength(); j++) {
									if (ccDataLayerChildNodeList.item(j).getNodeName().equalsIgnoreCase("SqlGroup")) {
										dlChildNodeToInsertBefore = ccDataLayerChildNodeList.item(j);
										lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Found SqlGroup");
									}
									// ...SqlCrosstab if working with an Additional Column with a Color.
									else if (ccDataLayerChildNodeList.item(j).getNodeName().equalsIgnoreCase("SqlCrosstab")) {
										dlChildNodeToInsertBefore = ccDataLayerChildNodeList.item(j);
										lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Found SqlCrosstab");
									}
								}

								// ...if SqlCalculatedColumn with ID = rdCustomColorColumn DNE
								// ... then add as child of DataLayer else remove any that exist
								NodeList ccSqlCalcColorColList = (NodeList) xpath.compile("//ChartCanvas/DataLayer/SqlCalculatedColumn[@ID='rdCustomColorColumn']").evaluate(doc, XPathConstants.NODESET);
								for (int k = 0; k < ccSqlCalcColorColList.getLength(); k++) {
									chartDataLayerNodeList.item(0).removeChild(ccSqlCalcColorColList.item(k));
								}

								// ...support configurable color matching using PluginParm
								String colorCaseStmt = "when 'Amber' then 'Yellow' when 'Clear' then 'Silver'";

								if (lgxObj.getPluginParameters().containsKey("rdCustomColorMap")) {
									colorCaseStmt = (String) lgxObj.getPluginParameters().get("rdCustomColorMap");
								}

								// ...new SqlCalculatedColumn
								Element newSqlCalculatedColumn = (Element) doc.createElement("SqlCalculatedColumn");
								newSqlCalculatedColumn.setAttribute("ID", "rdCustomColorColumn");
								newSqlCalculatedColumn.setAttribute("SqlFormula", "case COALESCE(@Data." + seriesXDataColumn + "~, 'NULL') " + colorCaseStmt + " else COALESCE(@Data." + seriesXDataColumn + "~, 'NULL') end");

								lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "dlChildNodeToInsertBefore = " + dlChildNodeToInsertBefore.getNodeValue());

								// ...insert before CC DataLayer's child node ... either SqlJoin or 1st child
								ccDataLayerNode.insertBefore((Node) newSqlCalculatedColumn, dlChildNodeToInsertBefore);

								if (seriesXDataColumnAttribute.equalsIgnoreCase("CrosstabColumn")) {

									// ...determine if the ExtraCrosstabValueColumn already exists
									NodeList ccSqlExtraCrosstabColList = (NodeList) xpath.compile("//ChartCanvas/DataLayer/SqlCrosstab/ExtraCrosstabValueColumn[@ID='" + sCrosstabCustomColorColumn + "']").evaluate(doc,
											XPathConstants.NODESET);

									if (ccSqlExtraCrosstabColList == null || ccSqlExtraCrosstabColList.getLength() == 0) {
										// ...only a single SqlCrosstab, place the ExtraCrosstabColumn under it.
										Element seriesElementSqlCrosstab = (Element) chartSeriesNodeListSqlCrosstab.item(0);

										Element newExtraCrosstabValueColumn = (Element) doc.createElement("ExtraCrosstabValueColumn");
										newExtraCrosstabValueColumn.setAttribute("ID", sCrosstabCustomColorColumn);
										newExtraCrosstabValueColumn.setAttribute("CrosstabValueColumn", "rdCustomColorColumn");
										newExtraCrosstabValueColumn.setAttribute("CrosstabValueFunction", "MIN");

										seriesElementSqlCrosstab.appendChild(newExtraCrosstabValueColumn);
									} else {
										// ...check to see how many and remove any duplicates.
										for (int k = 0; k < ccSqlExtraCrosstabColList.getLength() - 1; k++) {
											chartSeriesNodeListSqlCrosstab.item(0).removeChild(ccSqlExtraCrosstabColList.item(k));
										}
									}
								} else {
									// ...add SqlAggregateColumn to allow Color Data Column to be returned
									NodeList ccSqlGroupAggrColorColList = (NodeList) xpath.compile("//ChartCanvas/DataLayer/SqlGroup/SqlAggregateColumn[@ID='aggrCustomColorColumn']").evaluate(doc, XPathConstants.NODESET);
									if (ccSqlGroupAggrColorColList != null && ccSqlGroupAggrColorColList.getLength() == 0) {
										// ...create a new SqlAggregateColumn for the SqlCalculatedColumn
										// rdCustomColorColumn
										// ...select 1st SqlGroup from ChartCanvas DataLayer
										XPathExpression ccDlSqlGrpExpr = xpath.compile("//ChartCanvas/DataLayer/SqlGroup");
										NodeList chartDataLayerSqlGroupNodeList = (NodeList) ccDlSqlGrpExpr.evaluate(doc, XPathConstants.NODESET);

										int ccDlSqlGrpLen = chartDataLayerSqlGroupNodeList.getLength();
										if (ccDlSqlGrpLen > 0) {
											Element sqlGroupElement = (Element) chartDataLayerSqlGroupNodeList.item(0);

											// ...create a new SqlAggregateColumn for the ColorColumn
											if (sqlGroupElement != null) {
												Element newSqlAggregateColumn = (Element) doc.createElement("SqlAggregateColumn");

												// ... aggrColorColumn comes from the ID for the SqlColumn with
												// DataColumn =
												// ColorColumn
												newSqlAggregateColumn.setAttribute("AggregateColumn", "rdCustomColorColumn");
												newSqlAggregateColumn.setAttribute("AggregateFunction", "MIN");
												newSqlAggregateColumn.setAttribute("DataType", "Number");
												newSqlAggregateColumn.setAttribute("ID", "aggrCustomColorColumn");

												sqlGroupElement.appendChild(newSqlAggregateColumn);
											}
										}
									}
								}
							}
						}
						// ***************************************************************************
						// ***************************************************************************

						// ...set the correct attribute name for color from the Chart Series depending
						// on chart type
						// ...bar, line, scatter, spline all use attribute named Color
						String seriesColorAttribute = "Color";
						if (seriesElement.getAttribute("Type").equalsIgnoreCase("Pie")) {
							// ...pie uses attribute named Colors
							seriesColorAttribute = "Colors";
						}
						String seriesToken = "@Chart.aggrCustomColorColumn~";
						if (seriesXDataColumnAttribute.equals("CrosstabColumn")) {
							seriesToken = "@Chart." + sCrosstabCustomColorColumn + "~";
						}
						seriesElement.setAttribute(seriesColorAttribute, seriesToken);

						lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Chart Series Color set to " + seriesToken);
					}
				}

				// ...Save the modified Analysis Chart definition
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty("omit-xml-declaration", "yes");
				StringWriter writer = new StringWriter();
				transformer.transform(new DOMSource(doc), new StreamResult(writer));
				lgxObj.setCurrentDefinition(writer.toString());

				lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Post-Processing AnalysisChart Definition ...", lgxObj.getCurrentDefinition());

				// ...logic to allow logi engine to publish customized chart to Visual Gallery
				Element eleAnalysisChart = (Element) doc.getFirstChild();

				if (eleAnalysisChart != null) {
					String sid = doc.getDocumentElement().getAttribute("ID");

					HttpSession session = lgxObj.getSession();
					session.setAttribute("rdAcDef-" + sid, lgxObj.getCurrentDefinition());
					lgxObj.setSession(session);

					lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Session Var: rdAcDef-" + sid, lgxObj.getSession().getAttribute("rdAcDef-" + sid));
				}

			} else {
				lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Color column DONE!");
			}
		} catch (IOException | IllegalArgumentException | ParserConfigurationException | TransformerException | XPathExpressionException | DOMException | LSException | SAXException ex) {
			lgxObj.addDebugMessage("Plugin Call", "setColorsFromSelectedColumnWithTranslation", "Error: " + ex.getMessage(), ex);
		}
	}
}
