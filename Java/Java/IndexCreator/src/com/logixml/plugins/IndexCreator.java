package com.logixml.plugins;

//import org.w3c.dom.*;
//import org.w3c.dom.Document;
//import javax.servlet.*;
//import javax.servlet.http.*;
import com.logixml.plugins.LogiPluginObjects10;
//import java.util.*;
//import java.util.Hashtable;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
//import java.util.Scanner;
import java.io.File;

public class IndexCreator {

	public void indexCreator(LogiPluginObjects10 rdObjects) {
		FileWriter fWriter = null;
		FileReader fReader = null;
		BufferedWriter writer = null;
		BufferedReader reader = null;
		String thisLine = null;
		String UserName = null;
		// if (rdObjects.getPluginParameters().containsKey("UserName")) {
		// UserName = (String) rdObjects.getPluginParameters().get("UserName");
		// }
		UserName = "4e692f485049435c1818a5c7";
		String userHomeFolder = System.getProperty("user.dir");
		System.out.println("this is the home directory: " + userHomeFolder);
		System.out.println("this is the UserName: " + UserName);
		try {
			// FOR DOCKER CONTAINER
			// File UserCollectionCopy = new File (userHomeFolder + "goBookmarks"+
			// File.separator + "UserName" + UserName, UserName + "goCollection.xml");
			// File Collection = new File(userHomeFolder + File.separator + "_SupportFiles"
			// + File.separator + "Collection.txt");
			// HARD CODED DOCKER CONTAINER
			// File UserCollectionCopy = new File (File.separator + "usr" + File.separator +
			// "local" + File.separator + "tomcat" + File.separator + "webapps" +
			// File.separator + "RedSky" + File.separator + "goBookmarks"+ File.separator +
			// "UserName" + UserName, UserName + "goCollection.xml");
			// File Collection = new File(File.separator + "usr" + File.separator + "local"
			// + File.separator + "tomcat" + File.separator + "webapps" + File.separator +
			// "RedSky" + File.separator + "_SupportFiles" + File.separator +
			// "Collection.txt");
			// FOR LOCAL TESTING
			File Collection = new File(File.separator + File.separator + "Mac" + File.separator + "Home"
					+ File.separator + "Desktop" + File.separator + "Avetta" + File.separator + "biservice"
					+ File.separator + "webapps" + File.separator + "RedSky" + File.separator + "_SupportFiles"
					+ File.separator + "Collection.txt");
			File UserCollectionCopy = new File(File.separator + File.separator + "Mac" + File.separator + "Home"
					+ File.separator + "Desktop" + File.separator + "Avetta" + File.separator + "biservice"
					+ File.separator + "webapps" + File.separator + "RedSky" + File.separator + "goBookmarks"
					+ File.separator + "UserName" + UserName, UserName + "goCollection.xml");
			fReader = new FileReader(Collection);
			reader = new BufferedReader(fReader);
			fWriter = new FileWriter(UserCollectionCopy);
			writer = new BufferedWriter(fWriter);
			while ((thisLine = reader.readLine()) != null) {
				thisLine = thisLine.replaceAll("@Function.UserName~", UserName);
				System.out.println(thisLine);
				writer.write(thisLine + "\n");
			}
			writer.newLine();
		} catch (Exception e) {
			System.out.println("Error!");
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
				if (fWriter != null) {
					fWriter.close();
				}
				if (fReader != null) {
					fReader.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
				// ignore
			}
		}
	}

/**	public static void main(String[] args) {
		FileWriter fWriter = null;
		FileReader fReader = null;
		BufferedWriter writer = null;
		BufferedReader reader = null;
		String thisLine = null;
		String UserName = null;
		// if (rdObjects.getPluginParameters().containsKey("UserName")) {
		// UserName = (String) rdObjects.getPluginParameters().get("UserName");
		// }
		UserName = "4e692f485049435c1818a5c7";
		String userHomeFolder = System.getProperty("user.dir");
		System.out.println("this is the home directory: " + userHomeFolder);
		System.out.println("this is the UserName: " + UserName);
		try {
			// FOR DOCKER CONTAINER
			// File UserCollectionCopy = new File (userHomeFolder + "goBookmarks"+
			// File.separator + "UserName" + UserName, UserName + "goCollection.xml");
			// File Collection = new File(userHomeFolder + File.separator + "_SupportFiles"
			// + File.separator + "Collection.txt");
			// HARD CODED DOCKER CONTAINER
			// File UserCollectionCopy = new File (File.separator + "usr" + File.separator +
			// "local" + File.separator + "tomcat" + File.separator + "webapps" +
			// File.separator + "RedSky" + File.separator + "goBookmarks"+ File.separator +
			// "UserName" + UserName, UserName + "goCollection.xml");
			// File Collection = new File(File.separator + "usr" + File.separator + "local"
			// + File.separator + "tomcat" + File.separator + "webapps" + File.separator +
			// "RedSky" + File.separator + "_SupportFiles" + File.separator +
			// "Collection.txt");
			// FOR LOCAL TESTING
			File Collection = new File(File.separator + File.separator + "Mac" + File.separator + "Home"
					+ File.separator + "Desktop" + File.separator + "Avetta" + File.separator + "biservice"
					+ File.separator + "webapps" + File.separator + "RedSky" + File.separator + "_SupportFiles"
					+ File.separator + "Collection.txt");
			File UserCollectionCopy = new File(File.separator + File.separator + "Mac" + File.separator + "Home"
					+ File.separator + "Desktop" + File.separator + "Avetta" + File.separator + "biservice"
					+ File.separator + "webapps" + File.separator + "RedSky" + File.separator + "goBookmarks"
					+ File.separator + "UserName" + UserName, UserName + "goCollection.xml");
			fReader = new FileReader(Collection);
			reader = new BufferedReader(fReader);
			fWriter = new FileWriter(UserCollectionCopy);
			writer = new BufferedWriter(fWriter);
			while ((thisLine = reader.readLine()) != null) {
				thisLine = thisLine.replaceAll("@Function.UserName~", UserName);
				System.out.println(thisLine);
				writer.write(thisLine + "\n");
			}
			writer.newLine();
		} catch (Exception e) {
			System.out.println("Error!");
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
				if (fWriter != null) {
					fWriter.close();
				}
				if (fReader != null) {
					fReader.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
				// ignore
			}
		}
	}**/
}
