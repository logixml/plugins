package com.logianalytics.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;


import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.logixml.plugins.LogiPluginObjects10;

public class DataPlugin {

	@SuppressWarnings("resource")
	public void runDLPluginAsFile(LogiPluginObjects10 rdObjects) {
		try {
			String dataFile = rdObjects.getReturnedDataFile();

			// Identify data file.
			String xmlFile = rdObjects.getPluginParameters().get("dataFile").toString();

			FileChannel source = null;
			FileChannel destination = null;
			File input = new File(xmlFile);
			File output = new File(dataFile);

			// Simple copy from passed data file to the return data file.
			try {
				source = new FileInputStream(input).getChannel();
				destination = new FileOutputStream(output).getChannel();
				destination.transferFrom(source, 0, source.size());
			} finally {
				if (source != null) {
					source.close();
				}
				if (destination != null) {
					destination.close();
				}
			}

		} catch (Exception ex) {
		}
	}

	public void runDLPluginAsDoc(LogiPluginObjects10 rdObjects) {
		try {
			// Identify data file.
			String xmlFile = rdObjects.getPluginParameters().get("dataFile").toString();

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
					
			java.io.FileInputStream input = new FileInputStream(new File(xmlFile));
			Document dataXML = docBuilder.parse(input);
			
			input.close();
			
			rdObjects.setCurrentData(dataXML);
		} catch (Exception ex) {
		}
	}
}
