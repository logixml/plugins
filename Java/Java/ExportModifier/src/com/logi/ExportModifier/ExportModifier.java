package com.logi.ExportModifier;

//imports
import org.w3c.dom.*;
import com.logixml.plugins.LogiPluginObjects10;
import java.io.IOException;
import javax.xml.parsers.*;

import java.util.Hashtable;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;

public class ExportModifier {

	private String getOuterXml(Node node) {
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("omit-xml-declaration", "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			String result = writer.toString();
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Transformer Error " + ex.getMessage());
			return "Transformer getOuterXml Error " + ex.getMessage();
		}
	}

	public void runDeleteMasterReport(LogiPluginObjects10 rdObjects) {
		@SuppressWarnings("unchecked")
		Hashtable<String, String> ht = rdObjects.getPluginParameters();
		// Price Column name = plugin param
		String sColumnName = (String) ht.get("reportTypes");

		int iPos = rdObjects.getRequestParameterNames().indexOf((Object) "rdReportFormat");
		String sContinent = (String) rdObjects.getRequestParameterValues().get(iPos);
		try {
			// rdObjects.addDebugMessage("Started Try Remove Master");
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			byte b[] = rdObjects.getCurrentDefinition().getBytes();
			java.io.ByteArrayInputStream input = new java.io.ByteArrayInputStream(b);
			Document xmlSettings = docBuilder.parse(input);
			NodeList nl = xmlSettings.getElementsByTagName("MasterReport");
			int count = 0;
			while (nl.getLength() > 0) {
				// rdObjects.addDebugMessage("Length > 0");
				Node nodApp = nl.item(count);
				Element eleApp = (Element) nodApp;
				eleApp.getParentNode().removeChild(eleApp);
				count++;
			}
			rdObjects.setCurrentDefinition(getOuterXml(xmlSettings));
			// rdObjects.addDebugMessage("final 1", "final 2", "final 3",
			// xmlSettings);
		} catch (IOException | ParserConfigurationException | DOMException | SAXException ex) {
			rdObjects.addDebugMessage("Didn't work");
		}
	}

	public void runRestrictDatalayer(LogiPluginObjects10 rdObjects) {

		// Only run with the correct formats
		int iPos = rdObjects.getRequestParameterNames().indexOf("rdReportFormat");
		String reportFormat;
		if (iPos < 0) {
			// Not an export - exit
			rdObjects.addDebugMessage("", "rdReportFormat not found, not an export", "exiting");
			return;
		}
		reportFormat = (String) rdObjects.getRequestParameterValues().get(iPos);
		rdObjects.addDebugMessage("", "Parameter: rdReportFormat", reportFormat);

		if (!reportFormat.equalsIgnoreCase("CSV"))
			return;
		
		rdObjects.addDebugMessage("", "CSV export", "Modifying Max Rows");
		
		
		Document xmlDef;

		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			byte b[] = rdObjects.getCurrentDefinition().getBytes("UTF8");
			java.io.ByteArrayInputStream docInputStream = new java.io.ByteArrayInputStream(b);
			xmlDef = docBuilder.parse(docInputStream);

		} catch (IOException | ParserConfigurationException | DOMException | SAXException ex) {
			rdObjects.addDebugMessage("", "Error " + ex.getMessage());
			return;
		}

		rdObjects.addDebugMessage("", "Loaded Definition", "View Definition", getOuterXml(xmlDef));
		NodeList nl = xmlDef.getElementsByTagName("DataLayer");

		// Identify AG Max Rows limit and use this if available.
		String maxRowsExport = "100"; 
		
		try {
			maxRowsExport = rdObjects.replaceTokens("@Session.rdAgMaxRowsExport~").length() > 0 ? rdObjects.replaceTokens("@Session.rdAgMaxRowsExport~"): maxRowsExport;
		} catch (Exception e) {
			// Error parsing, keep default.
		}
		
		rdObjects.addDebugMessage("", "Setting Max Rows Value", maxRowsExport);
		if (nl != null) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element eleApp = (Element) nl.item(i);
				// @TODO update to use the Session token for AG Max Rows
							
				eleApp.setAttribute("MaxRows", maxRowsExport);
			}
		}

		rdObjects.setCurrentDefinition(getOuterXml(xmlDef));
		rdObjects.addDebugMessage("", "Analysis Grid Definition Updated", "View Definition", getOuterXml(xmlDef));
	}
	
	public void setMaxRowsDMF(LogiPluginObjects10 rdObjects) {

		// Only run with the correct formats
		int iPos = rdObjects.getRequestParameterNames().indexOf("rdReportFormat");
		String reportFormat;
		if (iPos < 0) {
			// Not an export - exit
			rdObjects.addDebugMessage("", "rdReportFormat not found, not an export", "exiting");
			return;
		}
		reportFormat = (String) rdObjects.getRequestParameterValues().get(iPos);
		rdObjects.addDebugMessage("", "Parameter: rdReportFormat", reportFormat);

		if (!reportFormat.equalsIgnoreCase("CSV"))
			return;
		
		rdObjects.addDebugMessage("", "CSV export", "Modifying Max Rows");
		
		
//		@SuppressWarnings("unchecked")
//		Hashtable<String, String> ht = rdObjects.getPluginParameters();
//		// Identify the theme from a Plugin Parameter
//		String theme = (String) ht.get("exportThemeName");
		
		Document xmlDef;

		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			byte b[] = rdObjects.getCurrentDefinition().getBytes("UTF8");
			java.io.ByteArrayInputStream docInputStream = new java.io.ByteArrayInputStream(b);
			xmlDef = docBuilder.parse(docInputStream);

		} catch (IOException | ParserConfigurationException | DOMException | SAXException ex) {
			rdObjects.addDebugMessage("", "Error " + ex.getMessage());
			return;
		}

		rdObjects.addDebugMessage("", "Loaded Definition", "View Definition", getOuterXml(xmlDef));
//		NodeList nl = xmlDef.getElementsByTagName("DefinitionModifierFile");

		// Identify AG Max Rows limit and use this if available.
//		String maxRowsExport = "100"; 
//		
//		try {
//			maxRowsExport = rdObjects.replaceTokens("@Session.rdAgMaxRowsExport~").length() > 0 ? rdObjects.replaceTokens("@Session.rdAgMaxRowsExport~"): maxRowsExport;
//		} catch (Exception e) {
//			 //Error parsing, keep default.
//		}
//		
//		rdObjects.addDebugMessage("", "Setting Max Rows Value", maxRowsExport);
		
		NodeList nl = xmlDef.getElementsByTagName("DefinitionModifierFile");
		// Just retrieve the first node in the list.
		Element eleApp = (Element) nl.item(0);
		eleApp.setAttribute("DefinitionModifierFile", "goCustomizations.dmfAnalysisGridExport.xml ");
		
//		if (nl != null) {
//			for (int i = 0; i < nl.getLength(); i++) {
//				Element root = (Element)nl.item(i);
				// @TODO update to use the Session token for AG Max Rows
				// Append a modified Theme to include Max Rows on ActiveSQL.
//				Element newTheme = xmlDef.createElement("StyleSheet");
//				newTheme.setAttribute("Theme", theme);
//				root.appendChild(newTheme);
//				
//
//			}
//		}

		rdObjects.setCurrentDefinition(getOuterXml(xmlDef));
		rdObjects.addDebugMessage("", "Export Theme Updated", "View Definition", getOuterXml(xmlDef));
	}

}