package com.logi.sample.plugins;

//import java.io.File;
//import java.util.Hashtable;
import org.w3c.dom.Document;
import org.w3c.dom.*;

//import java.util.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
//import org.xml.sax.SAXException;
//import org.xml.sax.SAXParseException;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.transform.OutputKeys;

import com.logixml.plugins.LogiPluginObjects10;

public class SamplePlugin {

	public void SetApplicationCaption(LogiPluginObjects10 rdObjects)
	{
		try
		{
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			byte b[] = rdObjects.getCurrentDefinition().getBytes("UTF8");
			java.io.ByteArrayInputStream input = new java.io.ByteArrayInputStream(b);
			Document xmlSettings = docBuilder.parse(input);
			NodeList nl = xmlSettings.getElementsByTagName("Application");
			if (nl.getLength() > 0)
			{
				Node nodApp = nl.item(0);
				Element eleApp = (Element)nodApp;
				eleApp.setAttribute("Caption", "Greetings from the Sample Plugin!");
				rdObjects.setCurrentDefinition(getOuterXml(xmlSettings));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			System.out.println("SetApplicationCaption Error " + ex.getMessage());
		}
	}
	
	public void setServerTimezone(LogiPluginObjects10 rdObjects) {
		TimeZone tz = TimeZone.getDefault();
		
		String zone = tz.getDisplayName(tz.inDaylightTime(new Date()), TimeZone.SHORT);
		
		try
		{
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			byte b[] = rdObjects.getCurrentDefinition().getBytes("UTF8");
			java.io.ByteArrayInputStream input = new java.io.ByteArrayInputStream(b);
			Document xmlSettings = docBuilder.parse(input);
			NodeList nl = xmlSettings.getElementsByTagName("Constants");
			if (nl.getLength() > 0)
			{
				Node nodApp = nl.item(0);
				Element eleApp = (Element)nodApp;
				eleApp.setAttribute("ServerTimeZone", zone);
				rdObjects.setCurrentDefinition(getOuterXml(xmlSettings));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			System.out.println("Set ServerTimeZone Error " + ex.getMessage());
		}
		
		rdObjects.addDebugMessage("", "Plugin TimeZone", "Constant Token ServerTimeZone set to " + zone);
	}
	
	public void setActiveSQLTimeZone(LogiPluginObjects10 rdObjects) {
		// Dynamically set the Timezone and apply it to the ActiveSQL query in SSRM using the GeneratedElementSqlPlugin Call.
		// @Input UserTimeZone - Provide the timezone for the end user to apply to the SQL query.
		String tz = "America/Chicago";
		
				
		if(rdObjects.getSession().getAttribute("UserTimeZone") != null && !rdObjects.getSession().getAttribute("UserTimeZone").toString().isEmpty()) 
			tz = rdObjects.getSession().getAttribute("UserTimeZone").toString();
		
		rdObjects.addDebugMessage("Active SQL Plugin", "", "Time Zone set to " + tz);
		
		
		String sqlQuery = "SET TIME ZONE '" + tz + "';\n" + rdObjects.getSQLQuery();
		
		rdObjects.setSQLQuery(sqlQuery);
		
	}
	
	private String getOuterXml(Node node)
	{
		try
		{
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("omit-xml-declaration", "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			String result = writer.toString();
			return result;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			System.out.println("Transformer Error " + ex.getMessage());
			return "Transformer getOuterXml Error " + ex.getMessage();
		}
	}
	
}
