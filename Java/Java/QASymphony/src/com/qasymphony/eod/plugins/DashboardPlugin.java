package com.qasymphony.eod.plugins;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.logixml.plugins.LogiPluginObjects10;

public class DashboardPlugin {

	public void setChartExportDimensions(LogiPluginObjects10 rdObjects) {
		
		int iReportFormat= rdObjects.getRequestParameterNames().indexOf((Object)"rdReportFormat");
		
		if(iReportFormat < 0 || !((String)rdObjects.getRequestParameterValues().get(iReportFormat)).equalsIgnoreCase("HtmlExport")) {
			// Not the HTML export - exit.
			rdObjects.addDebugMessage("Dashboard Export Plugin", "Not an Export call", "Exiting...");
			return;
		} 
		
		
				
		rdObjects.addDebugMessage("Dashboard Export Plugin", "Panel Definition", "View Definition",  rdObjects.getCurrentDefinition());
		
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			byte b[] = rdObjects.getCurrentDefinition().getBytes("UTF8");
			java.io.ByteArrayInputStream input = new java.io.ByteArrayInputStream(b);
			Document xmlPanel = docBuilder.parse(input);
			NodeList nl = xmlPanel.getElementsByTagName("ChartCanvas");
				
			String pnlStyle = xmlPanel.getDocumentElement().getAttribute("Style");
			
			rdObjects.addDebugMessage("", "Panel Style", pnlStyle);
			
			String chartHeight = pnlStyle.indexOf("height: ") > 0 ? pnlStyle.substring(pnlStyle.indexOf("height: ") + 8) : "";
			chartHeight = chartHeight.indexOf("px;") > 0 ? chartHeight.substring(0,  chartHeight.indexOf("px;")) : "400";
			
			String chartWidth = pnlStyle.indexOf("width: ") > 0 ? pnlStyle.substring(pnlStyle.indexOf("width: ") + 7) : "";
			chartWidth = chartWidth.indexOf("px;") > 0 ? chartWidth.substring(0,  chartWidth.indexOf("px;")) : "600";
			
			rdObjects.addDebugMessage("", "Chart Dimensions", "ChartHeight: " + chartHeight + "\nChartWidth: " + chartWidth);
			
			if (nl.getLength() > 0)
			{
				Node nodChart = nl.item(0);
				Element eleChart = (Element)nodChart;
				eleChart.setAttribute("ChartHeight", chartHeight);
				eleChart.setAttribute("ChartWidth", chartWidth);
				rdObjects.setCurrentDefinition(getOuterXml(xmlPanel));
			}
			

			rdObjects.addDebugMessage("Dashboard Export Plugin", "Updated Panel Definition", "View Definition",  rdObjects.getCurrentDefinition());
		} catch (Exception e) {	
			rdObjects.addDebugMessage("Dashboard Export Plugin", "Error extracting Chart Dimensions", e.getMessage());
		}
		
	}
	
	private String getOuterXml(Node node)
	{
		try
		{
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("omit-xml-declaration", "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			String result = writer.toString();
			return result;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			System.out.println("Transformer Error " + ex.getMessage());
			return "Transformer getOuterXml Error " + ex.getMessage();
		}
	}
}
