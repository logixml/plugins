
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SecureKeyRedirect
 */
@WebServlet("/SecureKeyRedirect")
public class SecureKeyRedirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String appIPAddr ="107.21.71.133";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("deprecation")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String LogiAppURL;
		String Username;
		String Roles;
		
		
		LogiAppURL = "http://" + appIPAddr + "/AdHoc";
		Username = "Jerry";
		Roles = "1";
		
		
		String getKeyURL = "/rdTemplate/rdGetSecureKey.aspx?Username=" + URLEncoder.encode(Username)
				+ "&Roles=" + URLEncoder.encode(Roles) + "&ahUserGroupID=1&ClientBrowserAddress=0.0.0.0";
				//				+ URLEncoder.encode(request.getRemoteAddr());

		String finalURL = LogiAppURL + getKeyURL;
		 
		// back-end handshake, passing username, roles and rights to Logi app
		URL url = new URL(response.encodeRedirectURL(finalURL));
		 
		// attempt to make SecureKey call
		try {
		   // open the connection
		   URLConnection conn = url.openConnection();
		   conn.setDoOutput(true);
		 
		   // debugging aid: use StreamWriter to write POST data
		   // OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		   // wr.write(data);
		   // wr.flush();
		   // wr.close();
		 
		   // get the response
		   BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		   StringBuffer sb = new StringBuffer();
		   String line;
		   while ((line = rd.readLine()) != null) {
		      sb.append(line);
		   }
		   rd.close();
		            
		   // set the url with the retrieved SecureKey parameter
		   String fnlLogiAppURL = LogiAppURL + "/Gateway.aspx?rdSecureKey=" + sb.toString();
		            
		   // redirect to the application with SecureKey parameter
		   response.sendRedirect(fnlLogiAppURL);
		}
		catch (Exception e) {
		  // throw new Exception("Failed to get SecureKey, request URL: [" + url + "]", e); //POST data: [" + data +"]", e);
			response.getWriter().println(e.getMessage());
			response.getWriter().println(e.getStackTrace().toString());
		}

		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.flushBuffer();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
