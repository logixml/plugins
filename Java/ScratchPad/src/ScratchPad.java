import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class ScratchPad {
	private static final char[] HEX = "0123456789abcdef".toCharArray();
	
public static void main(String[] args) {
	String json = null;
	String bytesToHex = convertBytesToHex("[TBFsC7BQRpSbW424iuQK6A==]".getBytes());
	 try {
	      byte[] bytes = Hex.decodeHex(bytesToHex.toCharArray());
	      json = new String(bytes, StandardCharsets.UTF_8.name());
	    } catch (UnsupportedEncodingException uee) {
	      System.out.println(uee.getMessage());
	    } catch (DecoderException dce) {
	    	System.out.println(dce.getMessage());
	    } catch (Exception e) {
	    	System.out.println("Exception: " + e.getMessage());
	    }
	 System.out.println(json);
}

private static String convertBytesToHex(byte[] value) {
    return convertBytesToHex(value, value.length);
  }
  
  private static String convertBytesToHex(byte[] value, int len) {
    char[] buff = new char[len + len];
    char[] hex = HEX;
    for (int i = 0; i < len; i++) {
      int c = value[i] & 0xFF;
      buff[i + i] = hex[c >> 4];
      buff[i + i + 1] = hex[c & 0xF];
    } 
    return new String(buff);
  }
	
//	public static void main(String[] args) {
//		System.out.println("Test");
//		int[] main = {10,5,2,4,3};
//		int[] part = {3,5,4,10};
//		ScratchPad pad = new ScratchPad();
//		
//		
//		long startTimeSimple = System.nanoTime();
//		int val = pad.missing_val(main, part);
//		long endTimeSimple = System.nanoTime();
//		long durationSimple = (endTimeSimple - startTimeSimple);
//		System.out.println("Simple Search value: " + val);
//		System.out.println("Simple search method took: " + durationSimple);
//		
//		long startTimeXor = System.nanoTime();
//		int val_xor = pad.missing_val_xor(main, part);
//		long endTimeXor = System.nanoTime();
//		long durationXor = (endTimeXor - startTimeXor);
//		System.out.println("XOR value: " + val_xor);
//		System.out.println("XOR search method took: " + durationXor);
//		
//	}
	
//	public ScratchPad ScratchPad() {}
//	
//	private int missing_val(int[] main, int[] partial) {
//		int missingVal = 0;
//				
//		for (int iNum : main) {
//			for (int i = 0; i < partial.length; i++) {
//				if (iNum != partial[i] && i == partial.length -1)
//					missingVal = iNum;
//				if (iNum == partial[i]) 
//					break;
//			}			
//		}
//		
//		return missingVal;
//	}
//	
//	private int missing_val_xor(int[] main, int[] partial) {
//		int missingVal = 0;
//		
//		 // 1st array is of size 'n' 
//        for (int i = 0; i < main.length; i++) 
//            missingVal = missingVal ^ main[i]; 
//      
//        // 2nd array is of size 'n - 1' 
//        for (int i = 0; i < partial.length; i++) 
//            missingVal = missingVal ^ partial[i]; 
//		
//		return missingVal;
//	}
	
}
