﻿using System;
using System.Xml;
using rdPlugin;

namespace DLPlugin_Schema
{
    public class LogiDataPlugin
    {
        public void DLPlugin_ToFile(ref rdServerObjects rdServer)
        {
            XmlDocument xDoc;
            String fname = (String)rdServer.PluginParameters["file"];

            xDoc = new XmlDocument();
            xDoc.Load(fname);

            xDoc.Save(rdServer.ReturnedDataFile); 

        }

        public void DLPlugin_ToDoc(ref rdServerObjects rdServer)
        {
            String fname = (String)rdServer.PluginParameters["file"];

            XmlDocument xDoc = rdServer.CurrentData;

            xDoc.Load(fname);
        }

    }
}
