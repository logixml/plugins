﻿namespace SignedPluginSample
{
    public class Plugin
    {
        public void simpleSignedSample(rdPlugin.rdServerObjects rdObjects)
        {
            rdObjects.AddDebugMessage("", "Signed Plugin", "Running");

            rdObjects.AddDebugMessage("", "Signed Plugin", "Completed");
        }

    }
}
