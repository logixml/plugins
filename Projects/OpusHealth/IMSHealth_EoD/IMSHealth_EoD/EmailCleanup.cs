﻿using System;
using System.Collections.Generic;
using rdPlugin;
using System.Web;
using System.Xml;
using System.Threading.Tasks;

namespace IMSHealth_EoD
{
    public class EmailCleanup
    {
        public void clearImages(rdServerObjects rdServer)
        {
            if (String.IsNullOrEmpty((String)rdServer.Request["rdReportFormat"]) || !(((String)rdServer.Request["rdReportFormat"]).Equals("HtmlEmail")))
            {
                rdServer.AddDebugMessage("", "Not occurring through Web Mail export.", "End Plugin Call");
                return;
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(rdServer.CurrentDefinition);

            rdServer.AddDebugMessage("Plugin Remove Images", "Current Definition", "View", xDoc);

            //Find the list of node Images to remove from Column.
            XmlNodeList nodes = xDoc.SelectNodes("//DataTableColumn//Image");
            rdServer.AddDebugMessage("","Number of Images found in DataTableColumns", nodes.Count.ToString());
            foreach (XmlNode node in nodes) {
                //The image is under a column that is no longer necessary...go up two levels and then remove the Image nodes parent.
                try
                {
                    node.ParentNode.ParentNode.RemoveChild(node.ParentNode);
                }
                catch (Exception e) { 
                    //This could cause an exception with both Images in the same parent, just ignore that error - the goal was accomplished.
                }
            }

            //Find the list of node Images to remove from the HeaderRow
            XmlNodeList hdrnodes = xDoc.SelectNodes("//HeaderRow//Image");
            rdServer.AddDebugMessage("","Number of Images found in DataTableColumns", nodes.Count.ToString());
            foreach (XmlNode node in hdrnodes) {
                node.ParentNode.RemoveChild(node);
            }

            rdServer.CurrentDefinition = xDoc.OuterXml;
        }

    }
}
