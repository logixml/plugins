﻿using rdPlugin;
using System;

namespace GeneratedSQLPlugin
{
    public class Plugin
    {
        public void ModifySQLPlugin(ref rdServerObjects rdObjects)
        {
            rdObjects.AddDebugMessage("Generated SQL Plugin", "", "Start");

            String sqlQueryString = rdObjects.SqlQuery;

            rdObjects.AddDebugMessage("Generated SQL Plugin", "", "View SQL", sqlQueryString);

            rdObjects.AddDebugMessage("Generated SQL Plugin", "", "End");
        }
    }
}
