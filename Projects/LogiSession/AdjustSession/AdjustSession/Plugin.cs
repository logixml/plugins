﻿using System;
using System.Web;
using rdPlugin;

namespace LogiPlugin
{
    public class Plugin
    {
        public void validateSession(rdServerObjects rdServer) {
            //Assign the session directly to the Session object in rdServer.
            rdServer.Session["TestSession"] = "My Updated Value";

            //Using a session HTTPSessionState object in code.
            System.Web.SessionState.HttpSessionState ss = rdServer.Session;
            ss["TestSession2"] = "My value from ss object";
        }
    }
}
