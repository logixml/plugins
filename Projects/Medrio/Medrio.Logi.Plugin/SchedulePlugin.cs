﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using TSHAK.Components;

namespace Medrio.Logi.Plugin
{
    public class SchedulePlugin
    {
        private const string StudyIdKey = "StudyId";
        private const string FileNameKey = "FileName";
        private const string DownloadUrlKey = "DownloadUrl";
        private const string ReportInfoIdKey = "ReportInfoID";
        private const string SavedReportIdKey = "SavedReportID";
        public void GetDownloadUrl(ref rdPlugin.rdServerObjects rdObjects)
        {
            try
            {
                Guid studyId = Guid.Parse((string)rdObjects.PluginParameters[StudyIdKey]);
                string fileName = (string)rdObjects.PluginParameters[FileNameKey];
                string reportInfoId = (string)rdObjects.PluginParameters[ReportInfoIdKey];
                string savedReportId = (string)rdObjects.PluginParameters[SavedReportIdKey];

                Directory.CreateDirectory(Path.GetDirectoryName(fileName));

                rdObjects.Session[(string)rdObjects.PluginParameters[DownloadUrlKey]] =
                    ConfigurationManager.AppSettings[DownloadUrlKey] + "?qs=" +
                    HttpUtility.UrlEncode(new SecureQueryString(studyId.ToByteArray()) { { FileNameKey, fileName }, { ReportInfoIdKey, reportInfoId }, { SavedReportIdKey, savedReportId } }.ToString());
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

    }
}
