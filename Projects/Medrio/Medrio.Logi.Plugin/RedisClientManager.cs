﻿using Medrio.Infrastructure.Redis;
using System;

namespace Medrio.Logi.Plugin
{
    public static class RedisClientManager
    {
        private static readonly Lazy<IRedisManager> redisManager;

        static RedisClientManager()
        {
            redisManager = new Lazy<IRedisManager>(() => RedisClient.Instance);
        }

        public static IRedisManager Instance
        {
            get
            {
                return redisManager.Value;
            }
        }
    }
}
