﻿using System;
using System.Configuration;
using System.Web;

namespace Medrio.Logi.Plugin
{
    public class WebConfigPlugin
    {
        public const string CurrentConnectionKey = "MedrioEtlCurrentConnectionKey";

        private const string DefaultConnectionKey = "DefaultConnectionName";
        private const string DownloadKey = "downloadKey";
        private const string MConsentConnectionName = "MConsent";
        private const string MConsentConnectionSessionKey = "mConsentConnectionSession";
        private const string SchedulerConnectionName = "Scheduler";
        private const string SchedulerConnectionSessionKey = "schedulerConnectionSession";
        private const string SqlConnectionSessionKey = "sqlConnectionSession";

        public void SetConfigurationValues(ref rdPlugin.rdServerObjects rdObjects)
        {
            try
            {
                rdObjects.Session[(string)rdObjects.PluginParameters[MConsentConnectionSessionKey]] = ConfigurationManager.ConnectionStrings[MConsentConnectionName]?.ConnectionString;
                rdObjects.Session[(string)rdObjects.PluginParameters[SchedulerConnectionSessionKey]] = ConfigurationManager.ConnectionStrings[SchedulerConnectionName]?.ConnectionString;

                string redisKey = ConfigurationManager.AppSettings[CurrentConnectionKey];
                string redisConnectionName = RedisClientManager.Instance.RedisDatabase.StringGet(redisKey);

                string connectionName = string.IsNullOrWhiteSpace(redisConnectionName)
                    ? ConfigurationManager.AppSettings[DefaultConnectionKey]
                    : redisConnectionName;

                string currentConnectionString = string.IsNullOrWhiteSpace(connectionName)
                    ? null : ConfigurationManager.ConnectionStrings[connectionName]?.ConnectionString;

                if (string.IsNullOrWhiteSpace(currentConnectionString))
                {
                    throw new ApplicationException($@"Invalid Logi database connection string.
                        redisKey: {redisKey}
                        redisConnectionName: {redisConnectionName}
                        connectionName: {connectionName}
                        currentConnectionString: {currentConnectionString}");
                }

                rdObjects.Session[(string)rdObjects.PluginParameters[SqlConnectionSessionKey]] = currentConnectionString;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public void GetDownloadKey(ref rdPlugin.rdServerObjects rdObjects)
        {
            rdObjects.Session[DownloadKey] = Guid.NewGuid().ToString("N");
        }

        public void SetDownloadCookie(ref rdPlugin.rdServerObjects rdObjects)
        {
            rdObjects.CurrentHttpContext.Response.AppendCookie(new HttpCookie(DownloadKey, rdObjects.Request[DownloadKey]));
        }

    }
}
