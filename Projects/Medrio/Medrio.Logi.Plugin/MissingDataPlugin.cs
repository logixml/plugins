﻿using rdPlugin;
using System.Text;
using System.Web;
using System.Xml;
using TSHAK.Components;

namespace Medrio.Logi.Plugin
{
    public class MissingDataPlugin
    {
        public void AddFormQuery(ref rdServerObjects rdObjects)
        {
            var key = (string)rdObjects.Session["EncryptionKey"];
            var accessableFormIds = (string)rdObjects.Session["AccessableFormIds"];

            if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(accessableFormIds))
                return;

            XmlDocument objDoc;
            XmlNodeList objNodeList;

            // load the XML from the datalayer
            objDoc = new XmlDocument();
            objDoc = rdObjects.CurrentData;

            // get a nodelist for all nodes (records) in XML doc
            // in XPath syntax "dtTest" equals the name of the data table, 
            // and is the node name in the XML document
            objNodeList = objDoc.SelectNodes("//dtMissingDataReport");

            // loop thru each node, processing the "colText" attribute value, 
            // which is name of column with RTF text.
            // put each value into RichTextBox as RTF, then reassign it as plain text
            foreach (XmlNode objNode in objNodeList)
            {
                var formId = objNode.Attributes.GetNamedItem("FormID").Value;

                var formLink = "";


                if (accessableFormIds.Contains(formId))
                {
                    formLink = GenerateSecuredQuery(key, objNode);
                }

                XmlAttribute newAttr = objDoc.CreateAttribute("FormLink");
                newAttr.Value = HttpUtility.UrlEncode(formLink);

                objNode.Attributes.Append(newAttr);
            }
        }

        private string GenerateSecuredQuery(string key, XmlNode objNode)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            var encryptionKey = encoding.GetBytes(key);

            var qs = new SecureQueryString(encryptionKey);

            var collectionPointId = objNode.Attributes.GetNamedItem("CollectionPt_ID").Value;

            if (!string.IsNullOrEmpty(collectionPointId))
            {
                qs.Add("collptid", collectionPointId);
            }

            return qs.ToString();
        }
    }
}

