﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medrio.Logi.Plugin
{
    public static class ExceptionLogger
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WebConfigPlugin));

        public static int LogException(Exception ex)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();

                StringBuilder exceptionTrace = new StringBuilder();

                //get list of exceptions
                IList<Exception> exList = new List<Exception>();
                Exception currEx = ex;
                exList.Add(currEx);

                while (currEx.InnerException != null)
                {
                    currEx = currEx.InnerException;
                    exList.Add(currEx);
                }

                for (int i = exList.Count - 1; i >= 0; i--)
                {
                    exceptionTrace.Append("\n Caused: \n");
                    exceptionTrace.Append(exList[i].Message);
                    exceptionTrace.Append(" \n");
                    exceptionTrace.Append(exList[i].StackTrace);
                }

                Log.Warn(exceptionTrace.ToString());

                return -1;
            }
            catch
            {
                return -1;
            }
        }
    }
}
