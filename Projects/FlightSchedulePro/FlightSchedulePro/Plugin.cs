﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.Logi.FlightSchedulePro
{
    public class Plugin
    {
        public void resolveMetadataUrl(rdPlugin.rdServerObjects rdObjects)
        {
            // Extract the current definition (_Settings)
            XmlDocument settings = new XmlDocument();
            settings.LoadXml(rdObjects.CurrentDefinition);

            /*
             * 
             * Identify the Metadata objects to resolve
             * This could be multiple - accept comma-delimited list
             * 
             */
            string metadataElements = string.IsNullOrEmpty((string)rdObjects.PluginParameters["metadata_ids"]) ? "" : rdObjects.PluginParameters["metadata_ids"].ToString();
            var metadata = metadataElements.Split(',');
            foreach (string ids in metadata) {
                string id = ids.Trim();
                XmlElement ele = (XmlElement) settings.SelectSingleNode("//Metadata[@ID='" + id + "']");
                if (ele != null)
                {
                    string value = "http://localhost/InfoGo12_7_348/@Session.locale~/md_Northwind.lgx"; //
                    value = rdObjects.ReplaceTokens(value);
                    rdObjects.AddDebugMessage("Metadata URL Plugin", "Metadata ID: " + id, value);
                    ele.SetAttribute("MetadataUrl", value);
                }
            }

            rdObjects.CurrentDefinition = settings.InnerXml;
            rdObjects.AddDebugMessage("Metadata URL Plugin", "", "Settings file", rdObjects.CurrentDefinition);
        }
    }
}
