﻿Imports System.Xml
Imports rdPlugin

Public Class Plugin
    Public Sub AddAnalysisChartDrillTo(ByRef rdObjects As rdPlugin.rdServerObjects)

        ' load the report definition XML
        Dim xmlDef As New XmlDocument()
        xmlDef.LoadXml(rdObjects.CurrentDefinition)

        Dim ChartList As XmlNodeList = xmlDef.SelectNodes("//ChartCanvas")
        Dim AddedColumns As New Collection


        For Each chart As XmlElement In ChartList
            Dim series As XmlElement = chart.SelectSingleNode("//Series") 'only selects first series

            Dim ColumnList As XmlNodeList = series.SelectNodes("//SqlColumn")
            Dim ChartDrillTo As XmlElement = xmlDef.CreateElement("ChartDrillTo")

            For Each Column As XmlElement In ColumnList
                Dim ColumnId As String = Column.GetAttribute("ID")
                If Not AddedColumns.Contains(ColumnId) Then
                    Dim DrillToColumn As XmlElement = xmlDef.CreateElement("DrillToColumn")

                    Dim AttrId As XmlAttribute = xmlDef.CreateAttribute("ID")
                    AttrId.Value = Column.GetAttributeNode("ID").Value

                    Dim AttrColumn As XmlAttribute = xmlDef.CreateAttribute("DataColumn")
                    AttrColumn.Value = Column.GetAttributeNode("ID").Value

                    Dim AttrDataType As XmlAttribute = xmlDef.CreateAttribute("DataType")
                    AttrDataType.Value = Column.GetAttributeNode("DataType").Value

                    Dim AttrHeader As XmlAttribute = xmlDef.CreateAttribute("Header")
                    AttrHeader.Value = Column.GetAttributeNode("Caption").Value

                    DrillToColumn.Attributes.Append(AttrId)
                    DrillToColumn.Attributes.Append(AttrColumn)
                    DrillToColumn.Attributes.Append(AttrDataType)
                    DrillToColumn.Attributes.Append(AttrHeader)

                    ChartDrillTo.AppendChild(DrillToColumn)

                    'keep track of items added
                    AddedColumns.Add(DrillToColumn, ColumnId)

                End If
            Next

            If ChartDrillTo.ChildNodes.Count > 0 Then
                series.AppendChild(ChartDrillTo)
            End If

        Next

        rdObjects.CurrentDefinition = xmlDef.OuterXml

        rdObjects.AddDebugMessage("Plug-in Message", "TEST", "TESTING")

    End Sub

End Class
