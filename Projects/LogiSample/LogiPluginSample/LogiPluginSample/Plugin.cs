﻿using System;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Xml;

namespace LogiPluginSample
{
    class Plugin
    {
        string val1;
        string val2;
        string val3;

        public void MultiReturns(rdPlugin.rdServerObjects logiObjects)
        {
            val1 = "Test 1";
            val2 = "Test 2";
            val3 = "Test 3";

        }

        public void ExportTest(rdPlugin.rdServerObjects rdObjects)
        {
            string export = rdObjects.Request.Params["exportFormat"];
            rdObjects.AddDebugMessage("Export Plugin", "export parameter", export);
            if (export != null && export.ToUpper().Equals("PDF"))
            {
                rdObjects.AddDebugMessage("Export Plugin", "", "Export Format Identified");
                HttpResponse resp = rdObjects.CurrentHttpContext.Response;
                resp.Clear();
                resp.ContentType = "application/pdf";
                resp.AddHeader("filename", "test.pdf");
                resp.BinaryWrite(System.Text.Encoding.ASCII.GetBytes(rdObjects.ResponseHtml));
                resp.Flush();
            } else
            {
                return;
            }
        }

        public void UpdateSettings(rdPlugin.rdServerObjects rdObjects)
        {
            HttpSessionState sess = rdObjects.Session;
            if ((String)sess["ran"] != "True")
            {
                sess["ran"] = "True";
                return;
            }
            sess["ran"] = "";
            XmlDocument settings = new XmlDocument();
            settings.LoadXml(rdObjects.CurrentDefinition);
            //XmlDocument getSettings = rdObjects.SettingsDefinition;

            rdObjects.AddDebugMessage("Plugin", "", "Current Definition", settings);
            //rdObjects.AddDebugMessage("Plugin", "", "Settings Definition", getSettings);
            using (StreamWriter sw = File.AppendText(@"C:\LogiReports\SupportQ4_2020\log.txt"))
            {
                sw.WriteLine("_Settings Plugin Called " + DateTime.Now.ToString());
            }
            XmlElement constant = settings.CreateElement("Constant");
            constant.SetAttribute("Plugin_Ran", DateTime.Now.ToString());
            settings.SelectSingleNode("//Setting").AppendChild(constant);
            rdObjects.CurrentDefinition = settings.OuterXml;
        }
    }
}
