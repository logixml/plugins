﻿using System;
using System.Xml;

namespace ZenPsycheBusinessTime
{
    public class Plugin
    {
        bool debug = false;

        /*
         * 
         * Calculations to work with 24/5 Support Schedule
         * Active hours are from Sunday 9 PM to Friday 9PM ET.
         * 
         */
        public void busHoursCalc(ref rdPlugin.rdServerObjects rdObjects, ref DateTime dtStart, ref DateTime dtEnd, ref double busHours)
        {
            // Based on the start and end times, calculate the business hours that have elapsed

            if (debug)
            {
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Original Start Date", dtStart.ToString());
            }

            /*
             * Start Time
             */

            //if Friday, and after 9 PM, set to Sunday 9PM (add 2 days)
            if (dtStart.DayOfWeek == DayOfWeek.Friday && dtStart.Hour >= 21)
            {
                DateTime dtStartNormalized = dtStart.AddDays(2);
                dtStart = new DateTime(dtStartNormalized.Year, dtStartNormalized.Month, dtStartNormalized.Day, 21, 0, 0, 0);
            }

            //if Saturday, set to Sunday 9 PM (add 1 day)
            if (dtStart.DayOfWeek == DayOfWeek.Saturday)
            {
                DateTime dtStartNormalized = dtStart.AddDays(1);
                dtStart = new DateTime(dtStartNormalized.Year, dtStartNormalized.Month, dtStartNormalized.Day, 21, 0, 0, 0);
            }

            //if Sunday, and before 9 PM, set to 9 PM
            if (dtStart.DayOfWeek == DayOfWeek.Sunday && dtStart.Hour < 21)
            {
                dtStart = new DateTime(dtStart.Year, dtStart.Month, dtStart.Day, 21, 0, 0, 0);
            }

            //debug - resulting Start Date
            if (debug)
            {
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Normalized Start Date", dtStart.ToString());
            }

            /*
             * End Time
             */

            //debug
            if (debug)
            {
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Original End Date", dtEnd.ToString());
            }


            //if Friday, and after 9 PM, set to Sunday 9PM (add 2 days)
            if (dtEnd.DayOfWeek == DayOfWeek.Friday && dtEnd.Hour >= 21)
            {
                DateTime dtEndNormalized = dtEnd.AddDays(2);
                dtEnd = new DateTime(dtEndNormalized.Year, dtEndNormalized.Month, dtEndNormalized.Day, 21, 0, 0, 0);
            }

            //if Saturday, set to Sunday 9 PM (add 1 day)
            if (dtEnd.DayOfWeek == DayOfWeek.Saturday)
            {
                DateTime dtEndNormalized = dtEnd.AddDays(1);
                dtEnd = new DateTime(dtEndNormalized.Year, dtEndNormalized.Month, dtEndNormalized.Day, 21, 0, 0, 0);
            }

            //if Sunday, and before 9 PM, set to 9 PM
            if (dtEnd.DayOfWeek == DayOfWeek.Sunday && dtEnd.Hour < 21)
            {
                dtEnd = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 21, 0, 0, 0);
            }

            //debug - resulting End Date
            if (debug)
            {
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Normalized End Date", dtEnd.ToString());
            }

            /*
             * 
             * Calculate the time between the two dates
             * 
             */

            // # of full weeks between the 2 dates
            long nWeeks = Convert.ToInt64(Math.Floor(dtEnd.Subtract(dtStart).TotalDays / 7));

            // # of whole days between the 2 dates
            long nDays = Convert.ToInt64(Math.Floor(dtEnd.Subtract(dtStart).TotalDays % 7));

            var nDaysCase = nDays;
            switch (nDaysCase)
            {
                case 6:
                    if (dtStart.DayOfWeek == DayOfWeek.Sunday)
                        nDays = 5;
                    else
                        nDays = 4;
                    break;
                case 5:
                    if (dtStart.DayOfWeek == DayOfWeek.Sunday || dtStart.DayOfWeek == DayOfWeek.Monday)
                        nDays = 4;
                    else
                        nDays = 3;
                    break;
                case 4:
                    if (dtStart.DayOfWeek == DayOfWeek.Sunday || dtStart.DayOfWeek == DayOfWeek.Monday)
                        nDays = 4;
                    else if (dtStart.DayOfWeek == DayOfWeek.Tuesday)
                        nDays = 3;
                    else
                        nDays = 2;
                    break;
                case 3:
                    if (dtStart.DayOfWeek == DayOfWeek.Sunday || dtStart.DayOfWeek == DayOfWeek.Monday || dtStart.DayOfWeek == DayOfWeek.Tuesday)
                        nDays = 3;
                    else if (dtStart.DayOfWeek == DayOfWeek.Wednesday)
                        nDays = 2;
                    else
                        nDays = 1;
                    break;
                case 2:
                    if (dtStart.DayOfWeek == DayOfWeek.Friday)
                        nDays = 0;
                    else if (dtStart.DayOfWeek == DayOfWeek.Thursday)
                        nDays = 1;
                    else
                        nDays = 2;
                    break;
                case 1:
                    if (dtStart.DayOfWeek == DayOfWeek.Friday)
                        nDays = 0;
                    else
                        nDays = 1;
                    break;
            }

            //Calculate # of minutes between the dates
            long nMinutes = Convert.ToInt64(Math.Floor(dtEnd.Subtract(dtStart).TotalMinutes % (24 * 60)));

            if (nMinutes < 0)
                nMinutes = 0;

            if (debug)
            {
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Days between dates", Convert.ToInt64(dtEnd.Subtract(dtStart).TotalDays).ToString());
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Minutes between dates", Convert.ToInt64(dtEnd.Subtract(dtStart).TotalMinutes).ToString());
                rdObjects.AddDebugMessage("busHoursCalc plugin", "nWeeks", nWeeks.ToString());
                rdObjects.AddDebugMessage("busHoursCalc plugin", "nDaysCase", nDaysCase.ToString());
                rdObjects.AddDebugMessage("busHoursCalc plugin", "nDays", nDays.ToString());
                rdObjects.AddDebugMessage("busHoursCalc plugin", "nMinutes", nMinutes.ToString());
            }

            //Total the business hours from the individual components
            busHours = (nWeeks * 24 * 5) + (24 * nDays) + (nMinutes / 60);

            if (debug)
                rdObjects.AddDebugMessage("busHoursCalc plugin", "Business Hours", busHours.ToString());
        }

        /*
         * 
         * Calculates the business time between 2 columns from the datalayer
         * 
         * INPUTS
         * startDate - column name for the start time
         * endDate - column name for the end time (DEPRECATED)
         * newColumnName - column name for the business time difference
         * 
         * OUTPUT
         * newColumnName is added to the datalayer
         * 
         */
        public void busHoursDiff(ref rdPlugin.rdServerObjects rdObjects)
        {
            // get the 'debug' Plugin Parameter to determine if this should show debug output
            Boolean.TryParse((String)rdObjects.PluginParameters["debug"], out debug);

            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "Start");

            // Access the Datalayer (Data is passed as XML Document)
            XmlDocument xmlData = rdObjects.CurrentData;

            // get the name of the new column
            string sColumnName = (string)rdObjects.PluginParameters["NewColumnName"];
            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "NewColumnName", sColumnName);

            // get the name of the start date column to use for the calculation
            string sStartDateColumn = (string)rdObjects.PluginParameters["startDate"];
            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "Start Date Column", sStartDateColumn);

            DateTime dtEnd = DateTime.SpecifyKind(DateTime.Now.ToUniversalTime(), DateTimeKind.Utc).ToLocalTime();
            //DateTime dtEnd = dtEndLocal.ToUniversalTime();
            //TimeSpan offset = dtEndLocal - dtEnd;
            //int offsetHrs = Convert.ToInt32(offset.TotalHours);

            // loop through the data to create the business hours
            foreach (XmlElement eleRow in xmlData.SelectNodes("/rdData/*"))
            {
                DateTime dtStart = DateTime.SpecifyKind(DateTime.Parse(eleRow.GetAttribute(sStartDateColumn)), DateTimeKind.Utc).ToLocalTime();

                double busHours = 0;
                busHoursCalc(ref rdObjects, ref dtStart, ref dtEnd, ref busHours);

                eleRow.SetAttribute(sColumnName, busHours.ToString());
            }

            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "Resulting Data", "View Data", xmlData);

            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "Completed");
        }

        /*
         * 
         * Calculates the business time between 2 columns from the datalayer
         * 
         * INPUTS
         * None (hardcoded to handle two columns: Case_Created and LastModifiedDate)
         * 
         * OUTPUT
         * Two new columns in the datalayer depicting the business hours for the input columns
         * (BusinessTime and LastTouch, respectively)
         * 
         */
        public void busHoursDiffCreatedAndModified(ref rdPlugin.rdServerObjects rdObjects)
        {

            // get the 'debug' Plugin Parameter to determine if this should show debug output
            Boolean.TryParse((String)rdObjects.PluginParameters["debug"], out debug);

            if (debug)
                rdObjects.AddDebugMessage("busHoursDiffCreatedAndModified plugin", "Start");

            // Access the Datalayer (Data is passed as XML Document)
            XmlDocument xmlData = rdObjects.CurrentData;

            DateTime dtEndLocal = DateTime.Now;
            DateTime dtEnd = dtEndLocal.ToUniversalTime();
            TimeSpan offset = dtEndLocal - dtEnd;
            double offsetHrs = offset.TotalHours;

            if (debug)
                rdObjects.AddDebugMessage("Time Zone Offset", "", offsetHrs.ToString());

            // loop through the data to create the business hours
            double busHours = 0;
            foreach (XmlElement eleRow in xmlData.SelectNodes("/rdData/*"))
            {
                // Configuration for Case_Created and BusinessTime results
                /*
                 * Convert UTC created_at to Local Time (EST)
                 */
                DateTime dtStart = DateTime.Parse(eleRow.GetAttribute("created_at")).AddHours(offsetHrs);
                //DateTime dtStart = DateTime.Parse(eleRow.GetAttribute("created_at"));

                /*
                 * Pass local end time to the calculation along with local start
                 */
                busHoursCalc(ref rdObjects, ref dtStart, ref dtEndLocal, ref busHours);

                eleRow.SetAttribute("BusinessTime", busHours.ToString());

                // Add hoursSinceCreated to dataset to be used in front-end display
                eleRow.SetAttribute("hoursSinceCreated", dtEndLocal.Subtract(dtStart).TotalHours.ToString());

                // Configuration for LastModifiedDate and LastTouch
                /*
                 * Convert UTC created_at to Local Time (EST)
                 */
                dtStart = DateTime.Parse(eleRow.GetAttribute("updated_at")).AddHours(offsetHrs);
                //dtStart = DateTime.Parse(eleRow.GetAttribute("updated_at"));

                /*
                 * Pass local end time to the calculation along with local start
                 */
                busHoursCalc(ref rdObjects, ref dtStart, ref dtEndLocal, ref busHours);

                eleRow.SetAttribute("LastTouch", busHours.ToString());

            }

            if (debug)
                rdObjects.AddDebugMessage("busHoursDiffCreatedAndModified plugin", "Completed");
        }

        /*
         * 
         * 
         * calculates the business time between consecutive rows for a single columns
         * assumes reverse order list, first end date is defaulted to NOW
         * 
         * INPUTS
         * RealTime - column name containing date
         * NewColumnName - column name to be added
         * 
         * OUTPUT
         * NewColumnName included in datalayer with business time difference between current and previous rows
         * 
         */
        public void CaseHistoryDiff(ref rdPlugin.rdServerObjects rdObjects)
        {
            // get the 'debug' Plugin Parameter to determine if this should show debug output
            Boolean.TryParse((String)rdObjects.PluginParameters["debug"], out debug);

            if (debug)
                rdObjects.AddDebugMessage("CaseHistoryDiff plugin", "Start");

            // Access the Datalayer (Data is passed as XML Document)
            XmlDocument xmlData = rdObjects.CurrentData;

            // get the name of the new column
            string sColumnName = (string)rdObjects.PluginParameters["NewColumnName"];
            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "NewColumnName", sColumnName);

            // get the name of the start date column to use for the calculation
            string sStartDateColumn = (string)rdObjects.PluginParameters["startDate"];
            if (debug)
                rdObjects.AddDebugMessage("busHoursDiff plugin", "Start Date Column", sColumnName);

            DateTime dtEndLocal = DateTime.Now;
            DateTime dtEnd = dtEndLocal.ToUniversalTime();
            TimeSpan offset = dtEndLocal - dtEnd;
            int offsetHrs = Convert.ToInt32(offset.TotalHours);

            // loop through the data to create the business hours
            foreach (XmlElement eleRow in xmlData.SelectNodes("/Data/*"))
            {
                DateTime dtStart = DateTime.Parse(eleRow.GetAttribute(sStartDateColumn));

                double busHours = 0;
                busHoursCalc(ref rdObjects, ref dtStart, ref dtEnd, ref busHours);

                eleRow.SetAttribute(sColumnName, busHours.ToString());

                dtEnd = DateTime.Parse(eleRow.GetAttribute(sStartDateColumn));
            }

            if (debug)
                rdObjects.AddDebugMessage("CaseHistoryDiff plugin", "Start");
        }

    }
}
