﻿using Newtonsoft.Json.Linq;

using log4net;

using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Linq;
using System.Threading;

namespace ZenPsyche
{
    public partial class zenConnector : System.Web.UI.Page
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static readonly HttpClient client = new HttpClient();
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext http = HttpContext.Current;
            HttpRequest req = http.Request;

            JObject body = null;
            StreamReader strm = null;
            try
            {
                strm = new StreamReader(Request.InputStream, Request.ContentEncoding);
                body = JObject.Parse(strm.ReadToEnd());
            }
            catch (Exception ex)
            {
                logger.Error("Error parsing request body: " + ex.Message);
                Context.Response.Clear();
                Context.Response.Write("{ \"error\": \"invalid body object received\" }");
                Context.Response.End();
            }
            finally
            {
                strm.Close();
                strm.Dispose();
            }

            String endpoint = null;
            if (body["endpoint"] != null)
            {
                endpoint = body["endpoint"].ToString();
            }
            else
            {
                logger.Error("Endpoint not identified in request body");
                Context.Response.Clear();
                Context.Response.Write("{ \"error\": \"endpoint required\" }");
                Context.Response.End();
            }
            //String flat_path = String.IsNullOrEmpty(body["flattener_path"].ToString()) ? "//" + endpoint : body["flattener_path"].ToString();
            String urlHost = body["url_host"] != null ? body["url_host"].ToString() : "";
            String urlPath = body["url_path"] != null ? body["url_path"].ToString() : "";
            String user = body["user"] != null ? body["user"].ToString() : "";
            String pass = body["pass"] != null ? body["pass"].ToString() : "";
            String auth = body["auth"] != null ? body["auth"].ToString() : "";
            if (String.IsNullOrEmpty(auth) && !String.IsNullOrEmpty(user))
                auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(user + ":" + pass));

            String entityType = body["entity_type"] != null ? body["entity_type"].ToString() : "";

            JObject json = null;

            switch (endpoint)
            {
                case "tickets":
                    json = retrieve_tickets(urlHost, urlPath, auth, entityType);
                    break;
                case "ticketSearch":
                    json = search_tickets(urlHost, urlPath, auth, entityType);
                    break;
                case "UserList":
                    json = user_list(urlHost, urlPath, auth, entityType);
                    break;
                case "OrgList":
                    json = org_list(urlHost, urlPath, auth, entityType);
                    break;
                default:
                    break;
            }


            Context.Response.Clear();
            Context.Response.Write(json.ToString());
            Context.Response.End();
        }

        private JObject search_tickets(String urlHost, String urlPath, String auth, String entityType)
        {
            int xRateRemaining, xRateLimit;
            logger.Info("Search Ticket endpoint: " + urlHost + "/" + urlPath);
            JObject jsonTickets = new JObject();
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(urlHost);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = httpClient.GetAsync(urlPath).Result;
                JObject result;
                try
                {
                    result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                }
                catch (Exception e)
                {
                    logger.Error(urlPath + " - Error parsing response from ticket search endpoint. " + e.Message);
                    throw new Exception(e.Message);
                }

                int count = (int)result["count"];
                logger.Info(urlPath + " - Total results: " + count);
                if (count < 1)
                {
                    JProperty empty_tickets = new JProperty(entityType,
                                new JArray()
                            );
                    jsonTickets.Add(empty_tickets);
                    return jsonTickets;
                }
                JValue nextPage = (JValue)result["next_page"];
                JArray jaTickets = new JArray();
                int pgCount = 1;
                do
                {
                    pgCount++;
                    JArray tickets = (JArray)result["results"];
                    foreach (JObject tkt in tickets.Children())
                    {
                        JObject ticket_info = new JObject();
                        ticket_info.Add("count", count);
                        foreach (var prop in tkt.Properties())
                        {
                            if (!prop.Name.Equals("custom_fields") && !prop.Name.Equals("fields"))
                                switch (prop.Name)
                                {
                                    case "id":
                                    case "created_at":
                                    case "updated_at":
                                    case "type":
                                    case "subject":
                                    case "requester_id":
                                    case "assignee_id":
                                    case "organization_id":
                                        ticket_info.Add(prop.Name, prop.Value);
                                        break;
                                    case "collaborator_ids":
                                        ticket_info.Add(prop.Name, prop.Value.ToString().Replace("[","").Replace("]", "").Replace("\r\n", "").Replace(" ", ""));
                                        break;
                                    case "follower_ids":
                                        ticket_info.Add(prop.Name, prop.Value.ToString().Replace("[", "").Replace("]", "").Replace("\r\n", "").Replace(" ", ""));
                                        break;
                                    case "status":
                                    case "priority":
                                        if (String.IsNullOrEmpty(prop.Value.ToString()))
                                            prop.Value = "-";
                                        String priority = textInfo.ToTitleCase(prop.Value.ToString());
                                        ticket_info.Add(prop.Name, priority);
                                        break;
                                    default:
                                        break;
                                }

                        }
                        foreach (JObject custom_field in tkt["custom_fields"])
                        {
                            switch (custom_field["id"].ToString())
                            {
                                case "360025016333":
                                    String caseReason = textInfo.ToTitleCase(custom_field["value"].ToString().Replace("_", " ").Replace("-", "/"));
                                    ticket_info.Add("case_reason", caseReason);
                                    break;
                                case "360025017613":
                                    ticket_info.Add("salesforce_case_id", custom_field["value"]);
                                    break;
                                case "360025009694":
                                    String prod = textInfo.ToTitleCase(custom_field["value"].ToString().Replace("_", " "));
                                    ticket_info.Add("product", prod);
                                    break;
                                case "360025009794":
                                    ticket_info.Add("product_version", custom_field["value"]);
                                    break;
                                case "360025010274":
                                    ticket_info.Add("build_version", custom_field["value"]);
                                    break;
                                case "360025010294":
                                    ticket_info.Add("sp_version", custom_field["value"]);
                                    break;
                                case "360025016993":
                                    String requestArea = textInfo.ToTitleCase(custom_field["value"].ToString().Replace("_", " "));
                                    ticket_info.Add("request_area", requestArea);
                                    break;
                                case "360025017033":
                                    ticket_info.Add("escalated", custom_field["value"]);
                                    break;
                                case "360025017553":
                                    String dataSourceType = textInfo.ToTitleCase(custom_field["value"].ToString().Replace("_", " "));
                                    ticket_info.Add("data_source_type", dataSourceType);
                                    break;
                                case "360024967413":
                                    ticket_info.Add("secondary_owner", custom_field["value"]);
                                    break;
                                default:
                                    break;
                            }

                        }
                        jaTickets.Add(ticket_info);
                    }

                    nextPage = (JValue)result["next_page"];
                    if (nextPage.ToString().Contains(urlHost) && pgCount <= 10)
                    {
                        int.TryParse(response.Headers.GetValues("x-rate-limit").First(), out xRateLimit);
                        int.TryParse(response.Headers.GetValues("x-rate-limit-remaining").First(), out xRateRemaining);

                        rateLimiter(xRateLimit, xRateRemaining, urlPath);

                        logger.Info(urlPath + " - Retrieving next page: " + nextPage.ToString());
                        response = httpClient.GetAsync(nextPage.ToString().Replace(urlHost, "")).Result;
                        result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    }
                    if (pgCount > 10)
                    {
                        // We have reached the max number of rows returned, return to the report.
                        logger.Info(urlPath + " - Max pages reached");
                        break;
                    }
                } while (nextPage.ToString().Contains(urlHost));

                jsonTickets.Add(new JProperty(entityType, jaTickets));
                return jsonTickets;
            }
        }

        private JObject user_list(String urlHost, String urlPath, String auth, String entityType)
        {
            int xRateRemaining, xRateLimit;
            logger.Info("User List endpoint: " + urlHost + "/" + urlPath);
            JObject jsonUsers = new JObject();


            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(urlHost);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = httpClient.GetAsync(urlPath).Result;
                JObject result;
                try
                {
                    result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                }
                catch (Exception e)
                {
                    logger.Error(urlPath + " - Error parsing response from user list endpoint. " + e.Message);
                    throw new Exception(e.Message);
                }

                int count = (int)result["count"];
                logger.Info(urlPath + " - Total results: " + count);
                if (count < 1)
                {
                    JProperty empty_users = new JProperty(entityType,
                                new JArray()
                            );
                    jsonUsers.Add(empty_users);
                    return jsonUsers;
                }
                JValue nextPage = (JValue)result["next_page"];
                JArray jaEndUsers = new JArray();
                do
                {
                    JArray endusers = (JArray)result["users"];
                    foreach (JObject usrs in endusers.Children())
                    {
                        JObject enduser_info = new JObject();
                        foreach (var prop in usrs.Properties())
                        {
                            switch (prop.Name)
                            {
                                case "id":
                                case "name":
                                case "external_id":
                                case "time_zone":
                                    enduser_info.Add(entityType + "_" + prop.Name, prop.Value);
                                    break;
                                default:
                                    break;
                            }

                        }
                        jaEndUsers.Add(enduser_info);
                    }
                    nextPage = (JValue)result["next_page"];
                    if (nextPage.ToString().Contains(urlHost))
                    {
                        int.TryParse(response.Headers.GetValues("x-rate-limit").First(), out xRateLimit);
                        int.TryParse(response.Headers.GetValues("x-rate-limit-remaining").First(), out xRateRemaining);

                        rateLimiter(xRateLimit, xRateRemaining, urlPath, sleep: 7000); //sleep for 7 seconds instead of 5 on Users

                        logger.Info(urlPath + " - Retrieving next page: " + nextPage.ToString());
                        response = httpClient.GetAsync(nextPage.ToString().Replace(urlHost, "")).Result;
                        result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    }
                } while (nextPage.ToString().Contains(urlHost));

                jsonUsers.Add(new JProperty(entityType, jaEndUsers));
                return jsonUsers;
            }
        }

        private JObject org_list(String urlHost, String urlPath, String auth, String entityType)
        {
            int xRateRemaining, xRateLimit;
            logger.Info("Organization List endpoint: " + urlHost + "/" + urlPath);
            JObject jsonOrgs = new JObject();


            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(urlHost);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.GetAsync(urlPath).Result;
                JObject result;
                try
                {
                    result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                }
                catch (Exception e)
                {
                    logger.Error(urlPath + " - Error parsing response from organization list endpoint. " + e.Message);
                    throw new Exception(e.Message);
                }

                int count = (int)result["count"];
                logger.Info(urlPath + " - Total results: " + count);
                if (count < 1)
                {
                    JProperty empty_users = new JProperty(entityType,
                                new JArray()
                            );
                    jsonOrgs.Add(empty_users);
                    return jsonOrgs;
                }
                JValue nextPage = (JValue)result["next_page"];
                JArray jaOrgs = new JArray();
                do
                {
                    JArray orgs = (JArray)result[entityType];
                    foreach (JObject usrs in orgs.Children())
                    {
                        JObject org_info = new JObject();
                        foreach (var prop in usrs.Properties())
                        {
                            switch (prop.Name)
                            {
                                case "id":
                                case "name":
                                    org_info.Add(entityType + "_" + prop.Name, prop.Value);
                                    break;
                                case "external_id":
                                    org_info.Add(entityType + "_" + prop.Name, prop.Value.ToString().Length >= 15 ? prop.Value.ToString().Substring(0, 15) : prop.Value.ToString());
                                    break;
                                default:
                                    break;
                            }

                        }
                        jaOrgs.Add(org_info);
                    }
                    nextPage = (JValue)result["next_page"];
                    if (nextPage.ToString().Contains(urlHost))
                    {
                        int.TryParse(response.Headers.GetValues("x-rate-limit").First(), out xRateLimit);
                        int.TryParse(response.Headers.GetValues("x-rate-limit-remaining").First(), out xRateRemaining);

                        rateLimiter(xRateLimit, xRateRemaining, urlPath);

                        logger.Info(urlPath + " - Retrieving next page: " + nextPage.ToString());
                        response = httpClient.GetAsync(nextPage.ToString().Replace(urlHost, "")).Result;
                        result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    }
                } while (nextPage.ToString().Contains(urlHost));

                jsonOrgs.Add(new JProperty(entityType, jaOrgs));
                return jsonOrgs;
            }
        }


        private JObject retrieve_tickets(String urlHost, String urlPath, String auth, String entityType)
        {
            int xRateRemaining, xRateLimit;
            logger.Info("Tickets endpoint: " + urlHost + "/" + urlPath);
            JObject jsonTickets = new JObject();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(urlHost);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = httpClient.GetAsync(urlPath).Result;
                JObject result;
                try
                {
                    result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                }
                catch (Exception e)
                {
                    logger.Error(urlPath + " - Error parsing response from tickets endpoint. " + e.Message);
                    throw new Exception(e.Message);
                }

                int count = (int)result["count"];
                logger.Info(urlPath + " - Total results: " + count);
                if (count < 1)
                {
                    JProperty empty_tickets = new JProperty(entityType,
                                new JArray()
                            );
                    jsonTickets.Add(empty_tickets);
                    return jsonTickets;
                }
                JValue nextPage = (JValue)result["next_page"];
                JArray jaTickets = new JArray();
                do
                {
                    JArray tickets = (JArray)result[entityType];
                    foreach (JObject tkt in tickets.Children())
                    {
                        JObject ticket_info = new JObject();
                        foreach (var prop in tkt.Properties())
                        {
                            if (!prop.Name.Equals("custom_fields") && !prop.Name.Equals("fields"))
                                ticket_info.Add(prop.Name, prop.Value);
                        }
                        foreach (JObject custom_field in tkt["custom_fields"])
                        {
                            switch (custom_field["id"].ToString())
                            {
                                case "360025016333":
                                    ticket_info.Add("case_reason", custom_field["value"]);
                                    break;
                                case "360025017613":
                                    ticket_info.Add("salesforce_case_id", custom_field["value"]);
                                    break;
                                case "360025009694":
                                    ticket_info.Add("product", custom_field["value"]);
                                    break;
                                case "360025009794":
                                    ticket_info.Add("product_version", custom_field["value"]);
                                    break;
                                case "360025010274":
                                    ticket_info.Add("build_version", custom_field["value"]);
                                    break;
                                case "360025010294":
                                    ticket_info.Add("sp_version", custom_field["value"]);
                                    break;
                                case "360025016993":
                                    ticket_info.Add("request_area", custom_field["value"]);
                                    break;
                                case "360025017033":
                                    ticket_info.Add("escalated", custom_field["value"]);
                                    break;
                                case "360025017553":
                                    ticket_info.Add("data_source_type", custom_field["value"]);
                                    break;
                                case "360024967413":
                                    ticket_info.Add("secondary_owner", custom_field["value"]);
                                    break;
                                default:
                                    break;
                            }

                        }
                        jaTickets.Add(ticket_info);
                    }

                    nextPage = (JValue)result["next_page"];
                    if (nextPage.ToString().Contains(urlHost))
                    {
                        int.TryParse(response.Headers.GetValues("x-rate-limit").First(), out xRateLimit);
                        int.TryParse(response.Headers.GetValues("x-rate-limit-remaining").First(), out xRateRemaining);

                        rateLimiter(xRateLimit, xRateRemaining, urlPath);

                        logger.Info(urlPath + " - Retrieving next page: " + nextPage.ToString());
                        response = httpClient.GetAsync(nextPage.ToString().Replace(urlHost, "")).Result;
                        result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    }
                } while (nextPage.ToString().Contains(urlHost));

                jsonTickets.Add(new JProperty(entityType, jaTickets));
                return jsonTickets;
            }

        }

        private void rateLimiter(int limit, int remaining, String urlPath, String msg = " - Requests near rate limit, pausing before requesting next page", double limiter = .1, int sleep = 5000)
        {
            logger.Info(urlPath + " - Rate Limit: " + limit + ", Limit Remaining: " + remaining);

            if (remaining <= (limit * limiter)) //If we are within {limiter} % of the rate limit, throttle
            {
                if (remaining <= (limit * (limiter / 2)))
                {
                    sleep = sleep * 2;
                    logger.Info(urlPath + " - rate within " + (limiter / 2 * 100) + "% of the limit, pausing for " + sleep / 1000 + "seconds");
                }
                logger.Info(urlPath + msg);
                Thread.Sleep(sleep); //Sleep for {sleep} seconds to delay the next request
            }
        }

    }
}