﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace metadata
{
    public partial class metadataProxy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpRequest req = HttpContext.Current.Request;

            string metaURL = "https://aurigologi.aurigoessentials.com/api/LOGIMetaData/GetMetadata"; // May need to be set up from a different location to pull dynamically.

            /*
             * 
             * Parameters passed:
             * 
             * sessionID - The UserSessionID from the Logi Session
             * uID - The users' uID from the Logi Session
             * rID - The RID from the Logi Session
             * 
             */
            var array = (
                from key in req.QueryString.AllKeys
                from value in req.QueryString.GetValues(key)
                select string.Format(
                    "{0}={1}",
                    HttpUtility.UrlEncode(key),
                    HttpUtility.UrlEncode(value))
            ).ToArray();

            string urlString = "";
            if (array.Length > 0)
                urlString = "?" + string.Join("&", array);
            string urlPath = metaURL + urlString;

            string xmlResp;

            // Create the request to extract the metadata
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; // Require TLS 1.2 connection
            using (var httpClient = new HttpClient())
            {
                xmlResp = httpClient.GetStringAsync(urlPath).Result;

            }

            Response.Clear();
            Response.Write(xmlResp);
            Response.End();
        }
    }
}