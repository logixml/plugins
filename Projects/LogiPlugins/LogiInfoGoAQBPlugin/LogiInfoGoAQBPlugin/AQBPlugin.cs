﻿using System;
using System.Xml;

namespace LogiInfoGoAQBPlugin
{
    public class AQBPlugin
    {

        public void updateAQB(rdPlugin.rdServerObjects rdObjects)
        {

            XmlDocument xmlDef = new XmlDocument(); 
            xmlDef.LoadXml(rdObjects.CurrentDefinition);

            // Debug message to provide information on location of plugin call.
            rdObjects.AddDebugMessage("Plugin Called", "Definition File", "View Definition", xmlDef);
        }

    }
}
