﻿using System;
using System.Collections.Generic;
using System.Xml;
using rdPlugin;

namespace ASqlPlugin_POC
{
    class ASQLTest
    {
        public void NewUnitPrice(rdServerObjects rdServer)
        {
            XmlDocument xData = rdServer.CurrentData;
            XmlNodeList xNodes;
            double price;
            
            // Get list of nodes without knowing the data object name
            xNodes = xData.SelectNodes("//*");

            foreach (XmlElement xElement in xNodes) {
                string val = xElement.GetAttribute("UnitPrice").ToString();
                Double.TryParse(val, out price);
                double newPrice = price * 1.35;
                xElement.SetAttribute("NewUnitPrice", newPrice.ToString());
            }
        }
    }
}
