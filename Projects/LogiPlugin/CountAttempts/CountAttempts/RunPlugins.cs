﻿using System;
using System.IO;
using System.Web;

using rdPlugin;

namespace CountAttempts
{
    public class RunPlugins
    {
        static String sFile = "Runs.txt";

        public void countPluginRuns(rdServerObjects rdServer) {
            TextWriter tw = new StreamWriter(rdServer.ReplaceTokens("@Function.AppPhysicalPath~\\" + sFile));
            tw.WriteLine("Plugin Executed" + "     " + DateTime.Now);
            tw.Close();          

        }

        public void nukeCacheHeaders(ref rdPlugin.rdServerObjects logiObj)
        {
            HttpContext httpContext = logiObj.CurrentHttpContext;
            StreamWriter sw = new StreamWriter(logiObj.ReplaceTokens("@Function.AppPhysicalPath~\\") + "logiheaders.log", true);

            sw.WriteLine("Cache: {0}", httpContext.Response.Cache);
            sw.WriteLine("Cache control: {0}", httpContext.Response.CacheControl);
            sw.WriteLine("Expires: {0}", httpContext.Response.Expires);
            sw.Close();
        } 

    }
}
