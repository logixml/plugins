﻿using System;
using System.Collections.Generic;
using System.Xml;
using rdPlugin;
using System.Linq;

namespace LogiPlugin
{
    public class SecurityRights
    {
        /*
         * 
         * Method to adjust the applied Security Rights within the application given a 
         * front-end web interface for administrators
         * 
         */
        public void adjustSecurityRights(rdServerObjects rdObjects)
        {
            /** 
             * 
             * Retrieve plugin parameters, should be two:
             * ReportName - Report Definition to be Updated
             * SecurityRights - Comma-delimited list of rights to be applied to the Report
             * 
             * Optional: 
             * Debug - If set, will turn debugging on
             * 
             **/
            String sReport = rdObjects.PluginParameters["ReportName"].ToString();
            String sRights = rdObjects.PluginParameters["SecurityRights"].ToString();

            // Optional - Turn on/off Debugging
            String sDebug = rdObjects.PluginParameters["Debug"].ToString();
            Boolean bDebug = !String.IsNullOrEmpty(sDebug) && sDebug.Equals("True") ? true : false;

            if (bDebug)
            {
                rdObjects.AddDebugMessage("Security Rights Plugin", "Start");
                rdObjects.AddDebugMessage("", "Retrieve Plugin Parameters");
                rdObjects.AddDebugMessage("", "ReportName", sReport);
                rdObjects.AddDebugMessage("", "SecurityRights", sRights);
            }


            /*
             * 
             * Not sure that I need the _Settings file currently
             *XmlDocument xSettings = rdObjects.SettingsDefinition; 
             *Debug to review the contents of the Settings file 
             * rdObjects.AddDebugMessage("", "", "Settings", xSettings.InnerXml.ToString());
             * 
             */


            // Get the physical path for the application
            String sPhysicalPath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath;

            // Locate the identified report
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(sPhysicalPath + "\\_Definitions\\_Reports\\" + sReport + ".lgx");
            }
            catch (Exception)
            {
                rdObjects.AddDebugMessage("", "Failed to retrieve report definition", "Aborting");
                return;
            }

            if (bDebug)
            {
                rdObjects.AddDebugMessage("", sReport, "View XML", xDoc);
            }

            XmlElement eleReport = (XmlElement)xDoc.SelectSingleNode("//Report");
            String currentRights = null;
            if (eleReport != null)
            {
                currentRights = eleReport.GetAttribute("SecurityReportRightID");

                if (!currentRights.Equals(sRights))
                {
                    if (bDebug)
                        rdObjects.AddDebugMessage("", "Updating Rights");

                    eleReport.SetAttribute("SecurityReportRightID", sRights);
                }
                else
                {
                    if (bDebug)
                        rdObjects.AddDebugMessage("", "Comparing Rights", "No changes necessary. Rights are identical to current report rights.");
                    return;
                }

                try
                {
                    // Will not work with Alternative Definitions Folder.
                    xDoc.Save(sPhysicalPath + "\\_Definitions\\_Reports\\" + sReport + ".lgx");
                }
                catch (Exception)
                {
                    if (bDebug)
                        rdObjects.AddDebugMessage("", "Unable to save changes", "Aborting");
                    return;
                }
            }
            else
            {
                if (bDebug)
                    rdObjects.AddDebugMessage("", "Could not find Report element", "Aborting");
                return;
            }

            if (bDebug)
            {
                rdObjects.AddDebugMessage("", "Report Rights Updated", "View Data", xDoc);
                rdObjects.AddDebugMessage("Security Rights Plugin", "Finished");
            }
        }
    }

    public class JSONString
    {
        public void getDataFromJSONString(rdServerObjects rdObjects)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(rdObjects.CurrentDefinition);

            String jsonData = rdObjects.PluginParameters["json"].ToString();
            if (!String.IsNullOrEmpty(jsonData))
            {
                JObject json = JObject.Parse();
            }
        }
    }
}
