﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SessionGuidPlugin
{
    public class Plugin
    {
        public void setVariableFromSessionGuid(rdPlugin.rdServerObjects rdServer)
        {
            // Extract Session Guid and save it as a string to Session variable. 
            String svarName = rdServer.PluginParameters["variable_name"].ToString();

            rdServer.Session.Add(svarName, rdServer.Session["rdSessionGUID"].ToString());
        }
    }
}
