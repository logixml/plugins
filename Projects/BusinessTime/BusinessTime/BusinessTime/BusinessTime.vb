﻿

Imports System.Configuration
Imports System.Xml
Imports System.Xml.Linq
Imports System.Web

Public Class Plugin

    Dim Debug As Boolean = False
    'Adjusting to 24/5 schedule for Support. 
    'Active from Sunday 9PM to Friday 9PM.

    Public Sub busHoursCalc(ByRef rdObjects As rdPlugin.rdServerObjects,
                        ByRef dtStart As Date, ByRef dtEnd As Date, ByRef busHours As Double)

        ' given start and end times, calculate the business hours elapsed
        ' 7am to 7pm (local)
        ' dont count weekends 

        If Debug Then
            rdObjects.AddDebugMessage("busHoursCalc plugin", "dtStart 1", dtStart.ToString)
        End If

        ' ' if after 7pm set to 7am next morning
        ' If dtStart.Hour >= 19 Then
        '     Dim dtStart2 As Date = dtStart.AddDays(1)
        '     dtStart = New DateTime(dtStart2.Year, dtStart2.Month, dtStart2.Day, 7, 0, 0, 0)
        ' End If

        ' ' if before 7am, set to 7am
        ' If dtStart.Hour < 7 Then
        '     Dim dtStart2 As Date = dtStart
        '     dtStart = New DateTime(dtStart2.Year, dtStart2.Month, dtStart2.Day, 7, 0, 0, 0)
        ' End If

        ' if Friday, and after 9pm, set to Sunday 9pm (add 2 days)
        If dtStart.DayOfWeek = DayOfWeek.Sunday AndAlso dtStart.Hour >= 21 Then
            Dim dtStart2 As Date = dtStart.AddDays(2)
            dtStart = New DateTime(dtStart2.Year, dtStart2.Month, dtStart2.Day, 21, 0, 0, 0)
        End If

        ' if Saturday, set to Sunday 9pm (add 1 days)
        If dtStart.DayOfWeek = DayOfWeek.Saturday Then
            Dim dtStart2 As Date = dtStart.AddDays(1)
            dtStart = New DateTime(dtStart2.Year, dtStart2.Month, dtStart2.Day, 21, 0, 0, 0)
        End If

        ' if Sunday, and before 9pm, set to Sunday 9pm (add 1 day)
        If dtStart.DayOfWeek = DayOfWeek.Sunday AndAlso dtStart.Hour < 21 Then
            ' Dim dtStart2 As Date = dtStart.AddDays(1)
            dtStart = New DateTime(dtStart.Year, dtStart.Month, dtStart.Day, 21, 0, 0, 0)
        End If

        If Debug Then
            rdObjects.AddDebugMessage("busHoursCalc plugin", "dtStart 2", dtStart.ToString)
        End If


        '
        ' end time        
        '


        If Debug Then
            rdObjects.AddDebugMessage("busHoursCalc plugin", "dtEnd 1", dtEnd.ToString)
        End If

        ' if after 7pm, set to 7pm
        ' If dtEnd.Hour >= 19 Then
        '     Dim dtEnd2 As Date = New DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 18, 59, 59, 0)
        '     dtEnd = dtEnd2
        ' End If

        ' if before 7am, set to 7pm day before
        ' If dtEnd.Hour < 7 Then
        '     Dim dtEnd2 As Date = dtEnd.AddDays(-1)
        '     dtEnd = New DateTime(dtEnd2.Year, dtEnd2.Month, dtEnd2.Day, 18, 59, 59, 0)
        ' End If

        'if Friday, after 9pm, set to Sunday 9pm (add a day)
        If dtEnd.DayOfWeek = DayOfWeek.Friday AndAlso dtEnd.Hour >= 21 Then
            Dim dtEnd2 As Date = dtEnd.AddDays(2)
            dtEnd = New DateTime(dtEnd2.Year, dtEnd2.Month, dtEnd2.Day, 21, 0, 0, 0)
        End If

        'if Saturday, set to Sunday 9pm (add a day)
        If dtEnd.DayOfWeek = DayOfWeek.Saturday Then
            Dim dtEnd2 As Date = dtEnd.AddDays(1)
            dtEnd = New DateTime(dtEnd2.Year, dtEnd2.Month, dtEnd2.Day, 21, 0, 0, 0)
        End If

        ' if Sunday, before 9pm, set to Sunday 9pm (subtract 2 days)
        If dtEnd.DayOfWeek = DayOfWeek.Sunday AndAlso dtEnd.Hour < 21 Then
            ' Dim dtEnd2 As Date = dtEnd.AddDays(-2)
            dtEnd = New DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 21, 0, 0, 0)
        End If

        If Debug Then
            rdObjects.AddDebugMessage("busHoursCalc plugin", "dtEnd 2", dtEnd.ToString)
        End If

        ' find # full weeks between 2 dates Nw
        Dim nWeeks As Long = DateDiff(DateInterval.Day, dtStart, dtEnd) \ 7

        ' find # whole days between 2 dates Nd (need remainder)
        Dim nDays As Long = DateDiff(DateInterval.Day, dtStart, dtEnd) Mod 7
        ' need to skip weekends dependiong on start day
        Dim nDaysCase = nDays
        Select Case nDaysCase
            Case 6
                If dtStart.DayOfWeek = DayOfWeek.Sunday Then
                    nDays = 5
                Else
                    nDays = 4
                End If
            Case 5
                If dtStart.DayOfWeek = DayOfWeek.Sunday Or dtStart.DayOfWeek = DayOfWeek.Monday Then
                    nDays = 4
                Else
                    nDays = 3
                End If
            Case 4
                If dtStart.DayOfWeek = DayOfWeek.Sunday Or dtStart.DayOfWeek = DayOfWeek.Monday Then
                    nDays = 4
                ElseIf dtStart.DayOfWeek = DayOfWeek.Tuesday Then
                    nDays = 3
                Else
                    nDays = 2
                End If
            Case 3
                If dtStart.DayOfWeek = DayOfWeek.Sunday Or dtStart.DayOfWeek = DayOfWeek.Monday Or dtStart.DayOfWeek = DayOfWeek.Tuesday Then
                    nDays = 3
                ElseIf dtStart.DayOfWeek = DayOfWeek.Wednesday Then
                    nDays = 2
                Else
                    nDays = 1
                End If
            Case 2
                If dtStart.DayOfWeek = DayOfWeek.Friday Then
                    nDays = 0
                ElseIf dtStart.DayOfWeek = DayOfWeek.Thursday Then
                    nDays = 1
                Else
                    nDays = 2
                End If
            Case 1
                If dtStart.DayOfWeek = DayOfWeek.Friday Then
                    nDays = 0
                Else
                    nDays = 1
                End If
        End Select

        ' find # minutes between 2 dates
        Dim nMinutes As Long = DateDiff(DateInterval.Minute, dtStart, dtEnd) Mod (24 * 60)

        ' If nMinutes > 12 * 60 Then
        '     nMinutes -= 12 * 60
        ' ElseIf nMinutes < 0 Then
        If nMinutes < 0 Then
            ' this can be -ve if Joe works a case before 7am
            nMinutes = 0
        End If

        If Debug Then
            rdObjects.AddDebugMessage("busHoursCalc plugin", "nWeeks", nWeeks)
            rdObjects.AddDebugMessage("busHoursCalc plugin", "nDaysCase", nDaysCase)
            rdObjects.AddDebugMessage("busHoursCalc plugin", "nDays", nDays)
            rdObjects.AddDebugMessage("busHoursCalc plugin", "nMinutes", nMinutes)
        End If

        busHours = (nWeeks * 24 * 5) + (24 * nDays) + (nMinutes / 60)
        If Debug Then
            rdObjects.AddDebugMessage("busHoursCalc plugin", "Business Hours", busHours)
        End If

    End Sub

    Public Sub busHoursDiff(ByRef rdObjects As rdPlugin.rdServerObjects)

        ' calculates the Business time between 2 columns in a datalayer in hours.
        '
        ' INPUTS 
        ' startDate - column name for start time
        ' endDate - column name for stop time
        ' NewColumnName - column name for business time difference

        ' OUTPUT
        ' NewColumnName is added to the datalayer


        If Debug Then
            rdObjects.AddDebugMessage("busHoursDiff plugin", "Start")
        End If

        ' access the data from the datalayer - in this case, the data is being 
        ' passed as an XML document ("Pass Data As" attribute)
        Dim xmlData As New XmlDocument()
        xmlData = rdObjects.CurrentData

        ' get name of the new column, which was passed as a plugin parameter
        Dim sColumnName As String = rdObjects.PluginParameters("NewColumnName")
        If Debug Then
            rdObjects.AddDebugMessage("busHoursDiff plugin", "NewColumnName", sColumnName)
        End If

        ' get name of date columns to use which were passed in as plugin parameters
        Dim sDateColumnName As String = rdObjects.PluginParameters("startDate")
        If Debug Then
            rdObjects.AddDebugMessage("busHoursDiff plugin", "startDate", sDateColumnName)
        End If

        'Dim eDateColumnName As String = rdObjects.PluginParameters("endDate")
        'If Debug Then
        'rdObjects.AddDebugMessage("busHoursDiff plugin", "endDate", eDateColumnName)
        'End If
        'endDate is always Now(), so we will calculate it once here instead of once per row in the Info engine.
        Dim dtEnd As Date = DateTime.Now

        ' iterate each data row, inserting the new column and its value
        Dim eleRow As XmlElement
        For Each eleRow In xmlData.SelectNodes("/rdData/*")

            ' start time
            Dim dtStart As Date = eleRow.GetAttribute(sDateColumnName)

            ' end time
            'Dim dtEnd As Date = eleRow.GetAttribute(eDateColumnName)

            Dim busHours As Double
            Call busHoursCalc(rdObjects, dtStart, dtEnd, busHours)
            ' set new column
            eleRow.SetAttribute(sColumnName, busHours)

        Next

    End Sub

    Public Sub CaseHistoryDiff(ByRef rdObjects As rdPlugin.rdServerObjects)

        ' calculates the business time between consecutive rows for a single column
        ' assumes reverse order list (so first end date is NOW)

        ' INPUTS
        ' RealTime - column name containing date
        ' NewColumnName - column name to be added 

        ' OUTPUT
        ' NewColumnName added to the datalayer with the Business time difference between this and the previous row

        If Debug Then
            rdObjects.AddDebugMessage("CaseHistoryDiff plugin", "Start")
        End If

        ' access the data from the datalayer - in this case, the data is being 
        ' passed as an XML document ("Pass Data As" attribute)
        Dim xmlData As New XmlDocument()
        xmlData = rdObjects.CurrentData

        ' get name of the new column, which was passed as a plugin parameter
        Dim sColumnName As String = rdObjects.PluginParameters("NewColumnName")
        If Debug Then
            rdObjects.AddDebugMessage("CaseHistoryDiff plugin", "NewColumnName", sColumnName)
        End If

        ' get name of date columns to use which were passed in as plugin parameters
        Dim sDateColumnName As String = rdObjects.PluginParameters("RealTime")
        If Debug Then
            rdObjects.AddDebugMessage("CaseHistoryDiff plugin", "RealTime", sDateColumnName)
        End If

        Dim dtEnd As Date = DateTime.Now

        ' iterate each data row, inserting the new column and its value
        Dim eleRow As XmlElement
        For Each eleRow In xmlData.SelectNodes("/rdData/*")

            ' start time
            Dim dtStart As Date = eleRow.GetAttribute(sDateColumnName)

            Dim busHours As Double
            Call busHoursCalc(rdObjects, dtStart, dtEnd, busHours)
            ' set new column
            eleRow.SetAttribute(sColumnName, busHours)

            ' set end date for next time
            dtEnd = eleRow.GetAttribute(sDateColumnName)

        Next

    End Sub

End Class
