﻿using System;
using System.IO;
using System.Diagnostics;

namespace Synapsify.ReportToPNG
{
    public class ExecuteScript
    {

        /**
         * Takes an HTML page and, using PhantomJS, implements a resulting image.
         * 
         * Parameters: 
         * URL - The URL of the source HTML to be converted (required)
         * phantomOPTS - The optional configuration options for running phantomJS
         * phantomLOC - The location of the phantomJS file, by default it will use the bin folder of the Logi application 
         * rasterLOC - The location of the resterize.js file, by default it will use the rdTemplate/rdChartCanvas folder of the Logi application
         * rasterFile - File name of the rasterize.js file
         * Timeout - Amount of time, in minutes, to allow the script to execute. (required)
         * OutputFolder - Folder location for resulting output image. (required)
         * OutputName - Filename for the resulting output file. (required)
         * 
         **/

        public void convertHTMLtoPNG(rdPlugin.rdServerObjects rdObjects)
        {
            String srcURL = (String)rdObjects.PluginParameters["URL"];
            String phantomOPTS = (String)rdObjects.PluginParameters["phantomOPTS"];
            String phantomLOC = (String)rdObjects.PluginParameters["phantomLOC"];
            String rasterLOC = (String)rdObjects.PluginParameters["rasterLOC"];
            String rasterFile = (String)rdObjects.PluginParameters["rasterFile"];
            String outputFolder = (String)rdObjects.PluginParameters["OutputFolder"];
            String outputName = (String)rdObjects.PluginParameters["OutputName"];

            int timeout = 0;
            int.TryParse(rdObjects.PluginParameters["Timeout"].ToString(), out timeout);

            // PhantomJS file location
            if (String.IsNullOrEmpty(phantomLOC))
            {
                // if the location was not provided, use the phantomjs present in the bin folder.
                phantomLOC = rdObjects.ReplaceTokens("@Function.AppPhysicalPath~") + ("\\bin\\");
            }
            else
            {
                // Check to see if the ending slash has been included.
                if (!phantomLOC.EndsWith("\\"))
                    phantomLOC = phantomLOC + "\\";
            }


            //Rasterize.js file location
            if (String.IsNullOrEmpty(rasterLOC))
            {
                // if the location was not provided, use the phantomjs present in the bin folder.
                rasterLOC = rdObjects.ReplaceTokens("@Function.AppPhysicalPath~") + ("\\rdTemplate\\rdChartCanvas\\");
            }
            else
            {
                // Check to see if the ending slash has been included.
                if (!rasterLOC.EndsWith("\\"))
                    rasterLOC = rasterLOC + "\\";
            }

            //Rasterize.js filename - may be a custom name
            if (String.IsNullOrEmpty(rasterFile))
            {
                // if the filename is empty, use rasterize.js as the default
                rasterFile = "rasterize.js";
            }
            
            rdObjects.AddDebugMessage("Convert HTML to Image Plugin", "Executing following URL", srcURL);

            Process runProc = null;


            if (!File.Exists(phantomLOC + "phantomjs.exe"))
                rdObjects.AddDebugMessage("Plugin Error", "Unable to locate phantomjs file.", phantomLOC + "phantomjs.exe");

            if (!File.Exists(rasterLOC + rasterFile))
                rdObjects.AddDebugMessage("Plugin Error", "Unable to locate " + rasterFile + " file.", rasterLOC + rasterFile);

            runProc = new Process();

            if (!String.IsNullOrEmpty(outputFolder))
                runProc.StartInfo.WorkingDirectory = outputFolder;


            String phantomCall = "\"" + phantomLOC + "phantomjs.exe" + "\" ";

            rdObjects.AddDebugMessage("", "Executing phantomjs", phantomCall + phantomOPTS + "\"" + rasterLOC + rasterFile + "\" " + srcURL + " " + outputName);

            runProc.StartInfo.FileName = phantomCall;
            runProc.StartInfo.Arguments = phantomOPTS + "\"" + rasterLOC + rasterFile + "\" " + srcURL + " " + outputName;
            runProc.StartInfo.RedirectStandardError = true;
            runProc.StartInfo.RedirectStandardOutput = true;
            runProc.StartInfo.UseShellExecute = false;

            runProc.Start();

            runProc.WaitForExit(
                (timeout <= 0) ? int.MaxValue : timeout * 1000 * 60
                );

            String errMessage = runProc.StandardError.ReadToEnd();
            runProc.WaitForExit();

            String outMessage = runProc.StandardOutput.ReadToEnd();
            runProc.WaitForExit();

            if (!String.IsNullOrEmpty(errMessage) && errMessage.Length > 0)
                rdObjects.AddDebugMessage("Plugin Error", "Error Occurred", errMessage);


            if (!String.IsNullOrEmpty(outMessage) && outMessage.Length > 0)
            {
                rdObjects.AddDebugMessage("Convert HTML to Image Plugin", "Execution Complete");
               // rdObjects.AddDebugMessage("Convert HTML to Image Plugin", "Output message from batch file.", outMessage);
            }

        }


    }
}
