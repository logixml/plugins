﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using rdPlugin;
using System.Text.RegularExpressions;

/**
 * 
 * Plugin for Anandarko Petroleum to take a pivoted data set, using the Logi Crosstab Filter
 * and clean the data so the value columns are named based on the column name from the pivoted Crosstab Column.
 * 
 * Change XML attribute names from the engine-generated rdCrosstab-X values to the name identified in the corresponding rdCrosstabColumn reference
 * Look for and resolve any potential XML encoding issues with the names provided
 * (optional) reduce the resulting dataset by cleaning up the unnecessary Crosstab Filter attributes created in the dataset
 * 
 **/

namespace LogiPlugin_AlterPivotData
{
    public class Plugin
    {
        public void AlterPivot(rdServerObjects rdObjects)
        {

            rdObjects.AddDebugMessage("Alter Pivot Data Plugin", "Initialized");

            // Allow the developer to select if the resulting XML file will be cleaned (may take extra processing time)
            bool bRemoveExtraAttrs = (rdObjects.PluginParameters.ContainsKey("CleanExtraAttributes") && rdObjects.PluginParameters["CleanExtraAttributes"].ToString().Equals("True")) ? true : false;

            // Define a prefix that can be applied to any columns that may start with invalid XML characters
            String prefix = (rdObjects.PluginParameters.ContainsKey("ColumnPrefix") && rdObjects.PluginParameters["ColumnPrefix"].ToString().Length > 0) ? rdObjects.PluginParameters["ColumnPrefix"].ToString() : "col_";

            rdObjects.AddDebugMessage("", "Clean Extra Attributes", bRemoveExtraAttrs.ToString());
            rdObjects.AddDebugMessage("", "Column Prefix Set", prefix);

            // Get the current dataset
            XDocument currData = ToXDocument(rdObjects.CurrentData);

            rdObjects.AddDebugMessage("", "Retrieved Dataset", "View Data", ToXmlDocument(currData));


            // rdCrosstabValueColumnCount - tells you how many columns are built by the pivot
            int numCols = 0;
            String[] cols = null;

            IEnumerable<XElement> ele = currData.Root.Elements();
            XElement elFirst = ele.First();
            // Just find the value in the first row for now.
            if (numCols == 0)
                Int32.TryParse(elFirst.Attribute("rdCrosstabValueColumnCount").Value, out numCols);

            // Initialize the column array after determining the column count from the XML.
            cols = new String[numCols];

            for (int i = 0; i < numCols; i++)
            {
                //Extract the column name and validate 
                String colName = elFirst.Attribute("rdCrosstabColumn-" + i).Value;
                String colClean = Regex.Replace(colName, "^[0-9][^0-9a-zA-Z]+", ""); //Remove any numeric characters in the first position of the string, and any special characters from the remaining string 
                if (String.IsNullOrEmpty(colClean) && String.IsNullOrEmpty(prefix))
                {
                    rdObjects.AddDebugMessage("", "Cleaned Column: " + colName, "Cleaned value is empty, skipping column");
                }
                else
                {
                    if (String.IsNullOrEmpty(prefix))
                    {
                        cols[i] = colClean;
                    }
                    else
                    {
                        cols[i] = prefix + colClean; //add the prefix to the cleaned value.
                    }
                }
            }


            rdObjects.AddDebugMessage("", "Number of Pivot Columns", numCols.ToString());
            rdObjects.AddDebugMessage("", "Columns list", String.Join(", ", cols));

            foreach (XElement el in ele)
            {

                for (int x = 0; x < numCols; x++)
                {

                    //Extract the column name and validate 
                    el.Add(new XAttribute(cols[x], el.Attribute("rdCrosstabValue-" + x).Value));

                }

                // break;
            }

            rdObjects.AddDebugMessage("", "Resulting XML", "View Data", ToXmlDocument(currData));


            // Load the updated XML data into the CurrentData Plugin object.
            rdObjects.CurrentData.LoadXml(currData.ToString());

        }

        private XDocument ToXDocument(XmlDocument document)
        {
            using (var nodeReader = new XmlNodeReader(document))
                return XDocument.Load(nodeReader);
        }

        private XmlDocument ToXmlDocument(XDocument xdoc)
        {
            var xmlDocument = new XmlDocument();

            using (var xmlReader = xdoc.CreateReader())
                xmlDocument.Load(xmlReader);

            return xmlDocument;
        }

    }
}
