﻿using System;
using System.Text;
using rdPlugin;
using System.Web;
using System.Xml;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace LogiEoD
{
    public class Authentication_SchedulerSetSession
    {

        /**
         * 
         * method: SetSession_Auth
         * Parameters:
         *      DebugEnabled - True or False to denote if debug messages should be added to the debug page.
         *      CodePairs - When set, will allow the dynamic addition of more pairs of valid HospitalSystemCodes with the associated Connection ID
         *          ie: TJHS:TJUH_Prod_Reporting,HHC:HHC_Prod_Reporting
         *      SessionVarName - Provides the Session token name to be created by the Plugin.
         * 
         **/
        public void SetSession_Auth(ref rdServerObjects rdServer)
        {
            bool _custCodes;

            // Identify debugging.
            bool _debug = String.IsNullOrEmpty((String)rdServer.PluginParameters["DebugEnabled"]) ? false : rdServer.PluginParameters["DebugEnabled"].ToString().ToUpper().Equals("TRUE");
            if (_debug) rdServer.AddDebugMessage("Authentication Scheduler Set Session Plugin", "", "Start");

            String sSessionVar = String.IsNullOrEmpty((String)rdServer.PluginParameters["SessionVarName"]) ? null : rdServer.PluginParameters["SessionVarName"].ToString();
            if (String.IsNullOrEmpty(sSessionVar))
            {
                if (_debug) rdServer.AddDebugMessage("", "Session Variable Name parameter is requred", "End Plugin Call");
                return;
            }

            /*
             * Only run when associated with Scheduler execution 
             * DatasourceId is custom session variable for RightCare Application.
             */
            if (String.IsNullOrEmpty((String)rdServer.Session[sSessionVar]))
            {
                if (String.IsNullOrEmpty((String)rdServer.Request["rdCaller"]) || !(((String)rdServer.Request["rdCaller"]).Equals("rdScheduler")))
                {
                    if (_debug) rdServer.AddDebugMessage("", "Not executed through Logi Scheduler", "End Plugin Call");
                    return;
                }
            }

            // Has dynamic CodePairs been set?
            String[] aHosCodes = String.IsNullOrEmpty((String)rdServer.PluginParameters["CodePairs"]) ? null : rdServer.PluginParameters["CodePairs"].ToString().Split(',');
            Dictionary<String, String> dCodes = new Dictionary<String, String>();
            if (aHosCodes != null)
            {
                foreach (String code in aHosCodes)
                {
                    String[] aCodes = code.Trim().Split(':');
                    try
                    {
                        dCodes.Add(aCodes[0], aCodes[1]);
                        if (_debug) rdServer.AddDebugMessage("", "Hospital System Code Added to List", aCodes[0] + " : " + dCodes[aCodes[0]]);
                    }
                    catch (IndexOutOfRangeException iore)
                    {
                        if (_debug) rdServer.AddDebugMessage("", "Invalid Code:Connection Pair - Skipping", "Verify that the CodePairs list contains <hositalcode>:<connection id> pairs separated by a comma");
                    }
                }
                _custCodes = true;
            }
            else
            {
                _custCodes = false;
            }


            // Retrieve RunAs from Scheduler call to identify user
            String sUser = String.IsNullOrEmpty(rdServer.Request["rdRunAs"]) ? String.Empty : rdServer.Request["rdRunAs"].ToString();

            if (String.IsNullOrEmpty(sUser))
            {
                if (_debug) rdServer.AddDebugMessage("", "User could not be determined.", "RunAs user required to create Session ID");
                return;
            }

            String sHosSysCode;
            // Split the HospitalSystemCode from the Username. Only the HospitalSystemCode will be needed to identify the DataSourceId.
            // Assumed user structure of <username>_<hospitalID> - abort if otherwise.
            try
            {
                sHosSysCode = (sUser.Split('_'))[1];
            }
            catch (IndexOutOfRangeException iore)
            {
                if (_debug) rdServer.AddDebugMessage("", "rdRunAs User not in correct format", "End Plugin Call");
                return;
            }

            
            if (_debug) rdServer.AddDebugMessage("", "Hospital System Code Identified", sHosSysCode);

            String sDataSourceId = null;
            if (_custCodes)
            {
                if (_debug) rdServer.AddDebugMessage("", "Using custom Hospital System Code list from parameters");
                sDataSourceId = dCodes[sHosSysCode];
            }
            else
            {
                switch (sHosSysCode)
                {
                    case "TJHS":
                        sDataSourceId = "TJUH_Prod_Reporting";
                        break;
                    case "HHC":
                        sDataSourceId = "HHC_Prod_Reporting";
                        break;
                    case "HMHS":
                        sDataSourceId = "HMHS_Prod_Reporting";
                        break;
                    case "SJMC":
                        sDataSourceId = "SJMC_Prod_Reporting";
                        break;
                    case "SNCH":
                        sDataSourceId = "SNCH_Prod_Reporting";
                        break;
                    case "UPHS":
                        sDataSourceId = "UPHS_Prod_Reporting";
                        break;
                    case "RRHS":
                        sDataSourceId = "Demo_Reporting";
                        break;
                    default:
                        sDataSourceId = "Unknown";
                        break;
                }
            }

            if (_debug) rdServer.AddDebugMessage("", sSessionVar + " set", sDataSourceId);

            rdServer.Session[sSessionVar] = sDataSourceId;

        }


    }
}
