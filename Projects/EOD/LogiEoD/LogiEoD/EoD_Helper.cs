﻿using System;
using System.Text;
using rdPlugin;
using System.Web;
using System.Xml;
using System.Collections.Generic;

namespace LogiEoD
{
    public class EoD_Helper
    {
        /**
         * 
         * Build a Datalayer XML file that includes a single column and a row for every 
         * numeric value between, and including, two end values.
         * 
         * Parameters
         * start - numeric value identifying the lower value of the range
         * end - numeric value identifying the upper value of the range
         * 
         **/
        public void datalayerRange(rdPlugin.rdServerObjects rdServer)
        {
            rdServer.AddDebugMessage("Datalayer Range Plugin", "Start", "");

            //Get the XML document for the datalayer.
            XmlDocument xDoc = rdServer.CurrentData;

            //Retrieve the Start and End Values from Plugin Parameters
            int nStart = -1;
            String sStart = String.IsNullOrEmpty((String)rdServer.PluginParameters["start"]) ? null : rdServer.PluginParameters["start"].ToString();
            sStart = rdServer.ReplaceTokens(sStart);
            Int32.TryParse(sStart, out nStart);

            rdServer.AddDebugMessage("", "start value", nStart.ToString());


            int nEnd = -1;
            String sEnd = String.IsNullOrEmpty((String)rdServer.PluginParameters["end"]) ? null : rdServer.PluginParameters["end"].ToString();
            sEnd = rdServer.ReplaceTokens(sEnd);
            Int32.TryParse(sEnd, out nEnd);

            rdServer.AddDebugMessage("", "start value", nEnd.ToString());


            if (nStart > nEnd || nStart == -1 || nEnd == -1)
            {
                rdServer.AddDebugMessage("", "Error in Plugin", "start and end values are incorrect, cannot complete operation.");
                return;
            }

            XmlNode root = xDoc.DocumentElement;
            for (int i = nStart; i <= nEnd; i++)
            {
                XmlElement eleRange = xDoc.CreateElement("dlRange");
                eleRange.SetAttribute("value", i.ToString());

                root.AppendChild(eleRange);
            }
        }
    }
}
