﻿using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using rdPlugin;
using System.Web;
using System.Threading.Tasks;
using System.Text;
using System.Threading;
using System.Net;

namespace LogiInfoPlugin
{
    public class Plugin
    {
        StringBuilder log = new StringBuilder();
        public void SetTenantDBName(rdServerObjects rdObjects)
        {
            //String strPluginCounter = (String)rdObjects.PluginParameters["PluginCounter"];
            String strPluginCounter = rdObjects.Session["PluginCounter"] == null ? "" : rdObjects.Session["PluginCounter"].ToString();

            rdObjects.AddDebugMessage("Incoming Plugin Counter", "Value is " + strPluginCounter);
            //strPluginCounter = strPluginCounter ?? "0";
            int intPluginCounter = string.IsNullOrEmpty(strPluginCounter) ? 0 : int.Parse(strPluginCounter);
            //int intPluginCounter = Int32.Parse(strPluginCounter);

            intPluginCounter++;
            rdObjects.AddDebugMessage("Incremented Plugin Counter", "New value is " + intPluginCounter);

            rdObjects.Session["PluginCounter"] = intPluginCounter;
            //rdObjects.Session.Remove("PluginCounter");
            //rdObjects.Session.Add("PluginCounter", intPluginCounter);

            rdObjects.AddDebugMessage("Saved Plugin Counter to Session", "Session value is " + rdObjects.Session["PluginCounter"].ToString());

            // Set the Database to the ClientID TenantID setting

            XmlDocument xmlSettings;
            XmlElement MultiTenantDB;

            //String strTenantID = (String)rdObjects.PluginParameters["TenantID"];
            String strTenantID = rdObjects.Session["TenantID"] == null ? "" : rdObjects.Session["TenantID"].ToString();
            rdObjects.AddDebugMessage("Incoming TenantID Session Var", "Value is " + rdObjects.Session["TenantID"]);
            rdObjects.AddDebugMessage("Incoming TenantID", "Value is " + strTenantID);
            strTenantID = string.IsNullOrEmpty(strTenantID) ? "tenant" : strTenantID;
            rdObjects.AddDebugMessage("Processed TenantID", "Value is " + strTenantID);

            // Load the current report definition into an XmlDocument object
            xmlSettings = new XmlDocument();
            xmlSettings.LoadXml(rdObjects.CurrentDefinition);
            rdObjects.AddDebugMessage("Settings","","Settings XML",xmlSettings.OuterXml);

            // Locate the Application element and define its Caption attribute
            MultiTenantDB = (XmlElement)xmlSettings.SelectSingleNode("//Setting/Connections/Connection[@ID='multitenant']");

            if ((MultiTenantDB != null) && MultiTenantDB.HasAttribute("ID"))
            {
                rdObjects.AddDebugMessage("Selected Connection", MultiTenantDB.Attributes["SqlServerDatabase"].Value);
                MultiTenantDB.SetAttribute("SqlServerDatabase", strTenantID);
                rdObjects.AddDebugMessage("Updated Selected Connection", MultiTenantDB.Attributes["SqlServerDatabase"].Value);
                // Save the new report definition
                rdObjects.CurrentDefinition = xmlSettings.OuterXml;
                rdObjects.AddDebugMessage("Settings", "Updated Tenant Connection", "Settings XML", xmlSettings.OuterXml);
            }
            else
            {
                rdObjects.AddDebugMessage("Error from Plugin", "Target connection element multitenant not found in _Settings");
            }
        }

        public void GetShortAgFilterDummy(ref rdServerObjects rdObjects)
        {
            string sAgTableID = (string)rdObjects.PluginParameters["AgTableID"];
            string sFilterDataID = (string)rdObjects.PluginParameters["FilterDataID"];
            string AndWord = (string)rdObjects.PluginParameters["AndWord"];
            string OrWord = (string)rdObjects.PluginParameters["OrWord"];
            string sResultAsSingleValue = (string)rdObjects.PluginParameters["ResultAsSingleValue"];
            sResultAsSingleValue = sResultAsSingleValue is null ? "true" : sResultAsSingleValue;
            string debugMode = (string)rdObjects.PluginParameters["DebugMode"];
            debugMode = debugMode is null ? "False" : debugMode;

            XmlDocument xmlData = rdObjects.CurrentData;
            XmlElement NewFilterRow;
            string RowValue = "";

            //Filter 1
            NewFilterRow = (XmlElement)xmlData.DocumentElement.AppendChild(xmlData.CreateElement(sFilterDataID));
            if (sResultAsSingleValue.ToLower() == "true")
            {
                RowValue += "[Tran Date] Date Range 2019 - 09 - 01 | 2021 - 02 - 03";
                NewFilterRow.SetAttribute("FilterValue", RowValue);
            }
            else
            {
                NewFilterRow.SetAttribute("Leader", "");
                NewFilterRow.SetAttribute("LeftParens", "");
                NewFilterRow.SetAttribute("FilterCaptionLimited", "[Tran Date] Date Range 2019 - 09 - 01 | 2021 - 02 - 03");
                NewFilterRow.SetAttribute("RightParens", "");
                NewFilterRow.SetAttribute("Disabled", "True");
            }

            //Filter 2
            NewFilterRow = (XmlElement)xmlData.DocumentElement.AppendChild(xmlData.CreateElement(sFilterDataID));
            if (sResultAsSingleValue.ToLower() == "true")
            {
                RowValue += "And (";
                RowValue += "[Alt Rep Name] >= Ana";
                NewFilterRow.SetAttribute("FilterValue", RowValue);
            }
            else
            {
                NewFilterRow.SetAttribute("Leader", "And ");
                NewFilterRow.SetAttribute("LeftParens", "(");
                NewFilterRow.SetAttribute("FilterCaptionLimited", "[Alt Rep Name] >= Ana");
                NewFilterRow.SetAttribute("RightParens", "");
            }

            //Filter 3
            NewFilterRow = (XmlElement)xmlData.DocumentElement.AppendChild(xmlData.CreateElement(sFilterDataID));
            if (sResultAsSingleValue.ToLower() == "true")
            {
                RowValue += "  And ";
                RowValue += "[AMOUNT] >= $0.00";
                RowValue += ")";
                NewFilterRow.SetAttribute("FilterValue", RowValue);
            }
            else
            {
                NewFilterRow.SetAttribute("Leader", "  And ");
                NewFilterRow.SetAttribute("LeftParens", "");
                NewFilterRow.SetAttribute("FilterCaptionLimited", "[AMOUNT] >= $0.00");
                NewFilterRow.SetAttribute("RightParens", ")");
            }


            //Filter 4
            NewFilterRow = (XmlElement)xmlData.DocumentElement.AppendChild(xmlData.CreateElement(sFilterDataID));
            if (sResultAsSingleValue.ToLower() == "true")
            {
                RowValue += "Or ";
                RowValue += "[insertid] < 100000";
                NewFilterRow.SetAttribute("FilterValue", RowValue);
            }
            else
            {
                NewFilterRow.SetAttribute("Leader", "Or ");
                NewFilterRow.SetAttribute("LeftParens", "");
                NewFilterRow.SetAttribute("FilterCaptionLimited", "[insertid] < 100000");
                NewFilterRow.SetAttribute("RightParens", "");
            }
            if ((debugMode.ToLower() == "true") || (debugMode.ToLower() == "after"))
                xmlData.Save("C:\\Temp\\AfterGetShortAGFilter_" + Guid.NewGuid().ToString() + ".xml");
        }

        public void runAsync(ref rdServerObjects server)
        {
            server.AddDebugMessage("Plugin", "Main Thread", "Initial Load");
            log.AppendLine("Starting runAsync Plugin");
            try
            {
                log.AppendLine("Inside Try - before Task.Run");
                var tsk = Task.Run(async () => await TestAsync().ConfigureAwait(false));
                while (!tsk.IsCompleted)
                {
                    Thread.Sleep(500);
                }
                log.AppendLine("Inside Try - after Task.Run");
            } catch(Exception ex)
            {
                server.AddDebugMessage("Plugin", "Error", ex.Message);
            }

            log.AppendLine("Task complete");
            File.AppendAllText("C:\\LogiReports\\SupportQ2_2021\\Log\\log.txt", log.ToString());
            log.Clear();
        }

        public async Task<string> TestAsync()
        {
            log.AppendLine("entered TestAsync");
            Random rndm = new Random();
            for (int i = 0; i < 5; i+=1)
            {
                log.AppendLine("testAsync logging: " + i);
                Thread.Sleep(2000 * rndm.Next(0, 1));
                try
                {
                    HttpWebRequest req = HttpWebRequest.CreateHttp("http://localhost");
                    var resp = await req.GetResponseAsync();
                }
                catch (Exception e) { }
            }

            return "Success";
        }
    }
}
