﻿using System;
using System.Web;
using System.Web.SessionState;
using System.IO;


namespace rdDownloadPageHandler
{
    public class rdDownloadSecurityPageHandler : IRequiresSessionState, IHttpHandler, IReadOnlySessionState
    {
        static string RDDOWNLOAD = "/rdDownload";
        public bool IsReusable => false;

        public void ProcessRequest(HttpContext context)
        {
            string sFilePath = "";
            string sExportFilePath = "";
            string sFileName = "";
            HttpRequest req = context.Request;
            HttpResponse resp = context.Response;

            try
            {
                // Extract the raw url for the request - provides file and type
                string sUrl = req.RawUrl;
                // parse the path to the file, looking for http(s)://<app>/rdDownload
                int iStartPos = sUrl.IndexOf(RDDOWNLOAD);
                // Map to a physical file path based on application location
                sFilePath = req.PhysicalApplicationPath;
                // Create the full path to file
                sFilePath += sUrl.Substring(iStartPos);
                sFilePath = sFilePath.Replace("\\", "/");
                int iLastSlash = sFilePath.LastIndexOf("/");
                sFileName = sFilePath.Substring(iLastSlash + 1);
                sExportFilePath = sFilePath.Substring(0, iLastSlash);

                // Check if the file has a Session ID and if the user is authorized
                if (!isAuthorizedAccess(sFileName) || !File.Exists(sFilePath))
                {
                    throw new Exception("403 Forbidden - You are not authorized to access these files.");
                }

            } catch(Exception ex)
            {
                string sErrorMsg = "<html><body style=\"margin-right: 0px; margin-left: 0px; font-size: 11px; font-family: verdana, arial, sans-serif;\"><div style=\"padding-left: 4px\"><h2><span style=\"color: #639e12; font-weight: normal; font-size: 18px;\">Info</span><span style=\" color: #3f60af; font-size: 18px;\"> Debugger Trace Report</span></h2></div> ";
                sErrorMsg += "<p style=\"color:Maroon; font-size:13pt; font-weight:bold; width: 100%;\"> There was an error while processing your request.</p>";
                sErrorMsg += "<p style=\"color:Maroon; font-size:13pt; font-weight:bold; width: 100%;\">The error was: " + ex.Message + "</p>";
                resp.Write(sErrorMsg);
                resp.End();
            }

            string sFilePathExt = null;
            string[] pathExts = { "pdf", "xls", "xlsx", "doc", "docx", "csv", "xml", "html", "htm", "zip" };

            // Identify filetype
            int iLastPeriod = sFileName.LastIndexOf(".");
            string sContentType = resp.ContentType;
            if (iLastPeriod > -1)
            {
                sFilePathExt = sFileName.Substring(iLastPeriod);
                sFilePathExt = sFilePathExt.Replace(".", "");
                // Find the appropriate Mime Type and set the Content Type
                sContentType = mimeType(sFilePathExt, sContentType);
            }

            if (File.Exists(sFilePath) && sFilePath.IndexOf("rdExport-") > 0)
            {

                // Found file and it is in a rdExport folder. We will delete the file if it is one of the 'export' filetypes

                // Send the response to the end user
                resp.Clear();
                resp.ContentType = sContentType;
                resp.WriteFile(sFilePath);
                resp.Flush();

                // File has been delivered, now delete the contents on disk.
                // Is the file an export file that should be deleted?
                if(sFilePathExt != null && Array.IndexOf(pathExts, sFilePathExt.ToLower()) >= 0)
                {
                    File.Delete(sFilePath);
                }
            }
            // File is still in rdDownload but not in an rdExport folder - process save/export?
            if (File.Exists(sFilePath) && sFilePath.IndexOf("rdExport-") <= 0)
            {
                // Found the requested file, but it is not part of an export - at least not in an rdExport- folder. 

                // Send the response to the end user
                resp.Clear();
                resp.ContentType = sContentType;
                resp.WriteFile(sFilePath);
                resp.Flush();

                if (sFileName.ToLower().StartsWith("rddl"))
                {
                    File.Delete(sFilePath);
                }

                string sDownload = req.PhysicalApplicationPath + RDDOWNLOAD;
                string sID = HttpContext.Current.Session.SessionID.ToLower();

                DirectoryInfo diExport = new DirectoryInfo(sExportFilePath);

                if (sDownload != sExportFilePath)
                {
                    FileInfo[] fiFilesExport = diExport.GetFiles("rdDL" + sID + "-");
                    foreach (FileInfo fiFileExport in fiFilesExport)
                    {
                        try
                        {
                            fiFileExport.Delete();
                        }
                        catch (Exception ex) { }
                    }
                }

                // Delete for any session png files attached to this process.
                FileInfo[] fiOtherFilesExport = diExport.GetFiles("rdDL" + sID + "*.*");
                foreach (FileInfo fiOtherFileExport in fiOtherFilesExport)
                {
                    try
                    {
                        fiOtherFileExport.Delete();
                    }
                    catch (Exception ex) { }
                }
            }
        }

        private bool isAuthorizedAccess(string sFileName)
        {
            if (!sFileName.ToLower().StartsWith("rddl"))
            {
                // No way to determine authorized session, default to true.
                return true;
            } else
            {
                string sID = HttpContext.Current.Session.SessionID.ToLower();
                if (sID.Length > 0)
                {
                    sID = "rddl" + sID + "-";
                    if (sFileName.ToLower().StartsWith(sID))
                    {
                        int iGuidLenExt = sFileName.Length - sID.Length;
                        if ((iGuidLenExt >= 35) && (iGuidLenExt <= 37))
                        {
                            // Session ID and Filename match, access granted
                            return true;
                        }
                    }
                    else
                    {
                        if (sFileName.ToLower().EndsWith(".png"))
                        {
                            // Pevent access errors to images, such as exported charts.
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private string mimeType(string format, string sMimeType)
        {
            string mimetype = sMimeType;
            format = format.ToLower();

            switch (format)
            {
                case "pdf":
                    mimetype = "application/pdf";
                    break;
                case "xls":
                    mimetype = "application/vnd.ms-excel";
                    break;
                case "xlsx":
                    mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "doc":
                    mimetype = "application/msword";
                    break;
                case "docx":
                    mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case "csv":
                    mimetype = "text/csv";
                    break;
                case "html":
                case "htm":
                    mimetype = "text/html";
                    break;
                case "xml":
                    mimetype = "text/xml";
                    break;
                case "zip":
                    mimetype = "application/zip";
                    break;
            }

            return mimetype;

        }
    }
}
