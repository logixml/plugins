﻿using System;
using System.Diagnostics;
using System.Xml;
using rdPlugin;

namespace Penta.Workbench.Plugin
{
    public class PluginMethods
    {
        // This plugin is based on code provided by LogiXML and was originally part of their InfoPlugin4 assembly.
        //
        // This plugin is used in conjunction with the larger solution to replace the "Edit Panel Params" embedded mode with the
        // better-performing "SubReport" mode that offloads all panel params to an iframe.
        public void ConfigPanelParamEditLinks(ref rdServerObjects rdObj)
        {
            // Load the definition into memory.
            XmlDocument xmlDashboard = new XmlDocument();
            xmlDashboard.LoadXml(rdObj.CurrentDefinition);

            // Determine the engine version of the definition.
            var versionInfo = FileVersionInfo.GetVersionInfo(rdObj.Request.PhysicalApplicationPath + @"bin\rdServer.dll");
            bool engineIs11_2plus = versionInfo.ProductMajorPart > 11 || (versionInfo.ProductMajorPart == 11 && versionInfo.ProductMinorPart >= 2);

            // We get the panel's instance ID from different elements depending on the engine version.
            if (engineIs11_2plus)
            {
                foreach (XmlElement elePopupOption in xmlDashboard.SelectNodes("//PopupOption[starts-with(@ID, 'ppoEdit')]"))
                {
                    XmlElement eleTarget = (XmlElement)elePopupOption.SelectSingleNode(".//Target[@ID='tgtRenamepanel']");
                    eleTarget.SetAttribute("Link", eleTarget.GetAttribute("Link").Replace("{PanelInstanceID}", elePopupOption.GetAttribute("ID").Substring(elePopupOption.GetAttribute("ID").Length - 32)));
                }
            }
            else
            {
                foreach (XmlElement eleDivision in xmlDashboard.SelectNodes("//Division[starts-with(@ID, 'rdDashboardEdit')]"))
                {
                    XmlElement eleTarget = (XmlElement)eleDivision.SelectSingleNode(".//Target[@ID='targetEditPanel']");
                    eleTarget.SetAttribute("Link", eleTarget.GetAttribute("Link").Replace("{PanelInstanceID}", eleDivision.GetAttribute("ID").Substring(eleDivision.GetAttribute("ID").Length - 32)));
                }
            }

            // Save the changes to the dashboard.
            rdObj.CurrentDefinition = xmlDashboard.DocumentElement.OuterXml;
        }

    }
}
