﻿using System;
using System.Configuration;
using System.Xml;
using rdPlugin;

namespace Penta.Workbench.Plugin
{
   public class ConnectionString
   {
      public void FillConnString(ref rdServerObjects lgxObj)
      {
         var connectionId = lgxObj.PluginParameters["ConnID"].ToString();
         lgxObj.AddDebugMessage("", "Connection ID", connectionId);

         var reportDef = GetCurrentReportDefinition(lgxObj);
         var connectionString = GetConnectionString(lgxObj);

         XmlElement connectionElement = (XmlElement)reportDef.SelectSingleNode("//*[@ID='" + connectionId + "']");
         connectionElement.SetAttribute("ConnectionString", connectionString);

         //DONT FORGET!! Save your changes
         lgxObj.CurrentDefinition = reportDef.DocumentElement.OuterXml;
      }

      private XmlDocument GetCurrentReportDefinition(rdServerObjects lgxObj)
      {
         var reportDef = new XmlDocument();
         reportDef.LoadXml(lgxObj.CurrentDefinition);
         lgxObj.AddDebugMessage("", "GetCurrentReportDefinition", "Obtained the current definition");

         return reportDef;
      }

      private string GetConnectionString(rdServerObjects lgxObj)
      {
         var connectionName = GetConnectionStringName(lgxObj);

         var connectionString = ConfigurationManager.ConnectionStrings[connectionName];
         if (connectionString != null)
         {
            lgxObj.AddDebugMessage("", "Connection String", "Found the connection string");
            return connectionString.ConnectionString;
         }
         else
         {
            lgxObj.AddDebugMessage("", "Connection String", String.Format("A connection named '{0}' could not be found.  Check the workbench's Web.Config.", connectionName));
            return null;
         }

      }

      private string GetConnectionStringName(rdServerObjects lgxObj)
      {
         String name = "PentaWorkbench";
         var connStrParm = lgxObj.PluginParameters["ConnectionStringName"];

         if (connStrParm != null && !String.IsNullOrWhiteSpace(connStrParm.ToString()))
         {
            name = connStrParm.ToString();
         }

         lgxObj.AddDebugMessage("", "GetConnectionStringName", name);

         return name;
      }
   }
}
