﻿using System;
using System.Configuration;
using System.IO;
using System.Xml;
using rdPlugin;
using System.Xml.Linq;

namespace Penta.Workbench.Plugin
{
    public class SetDefaultLayout
    {
        //set customcolumn2 = 3 for current user, current report, and current version of report
        //CurrentUser, CurrentReport, CurrentVersionReport, BookmarkLocation, BookmarkCollection

        //<Bookmark Report="ProjectManagement.Reports.LaborCostAnalysis" Name="Cost Summary Report" CustomColumn1="PENTA" CustomColumn2="" Description="Most Recent Bookmark Test 2" SaveTime="2014-10-29T14:08:21-05:00" BookmarkID="3ed177ff-da4e-465d-878b-613d2883eec7" ExtraFile="UserBookmarks_3ed177ff-da4e-465d-878b-613d2883eec7.xml">
        //Report = currentReport; Name = currentVersionReport; BookmarkID = bookmarkID
        
        public void SetBookmarkAsDefaultLayout(ref rdServerObjects lgxObj)
        {
            XmlDocument xmlBookmarkFile = new XmlDocument();

            String bookmarkFilePath = lgxObj.PluginParameters["BookmarkLocation"].ToString();
            String bookmarkID = lgxObj.PluginParameters["BookmarkID"].ToString(); 
            String checkedValue = lgxObj.PluginParameters["CheckedValue"].ToString();

                // ...verify file exists ... probably need to verify it is writable as well
                if (File.Exists(bookmarkFilePath))
                {
                    try{

                        xmlBookmarkFile.Load(bookmarkFilePath);
                        // ...Grab bookmark to clear
                        XmlNodeList selectBookmarkToDefault = xmlBookmarkFile.SelectNodes("//rdBookmarks/Bookmark[@BookmarkID='" + bookmarkID + "']");

                        if (selectBookmarkToDefault != null)
                        {

                            foreach (XmlNode bookmarkNode in selectBookmarkToDefault)
                            {
                                XmlElement bookmarkElement = (XmlElement)bookmarkNode;

                                if (!bookmarkElement.IsEmpty)
                                {                                                   //3
                                    bookmarkElement.SetAttribute("CustomColumn2", checkedValue);
                                    xmlBookmarkFile.Save(bookmarkFilePath);
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }              
                }

        }
        public void ClearBookmarkAsDefaultLayout(ref rdServerObjects lgxObj)
        {
            XmlDocument xmlBookmarkFile = new XmlDocument();

            String currentUser = lgxObj.PluginParameters["CurrentUser"].ToString();
            String currentReport = lgxObj.PluginParameters["CurrentReport"].ToString();
            String bookmarkFilePath = lgxObj.PluginParameters["BookmarkLocation"].ToString();
            String reportName = lgxObj.PluginParameters["ReportName"].ToString(); 

                // ...verify file exists ... probably need to verify it is writable as well
                if (File.Exists(bookmarkFilePath))
                {
                     try
                    {
                        xmlBookmarkFile.Load(bookmarkFilePath);

                        // ...Grab bookmark to clear w/ Report = currentReport and CustomColumn1= currentUser
                        XmlNodeList selectBookmarkToClear = xmlBookmarkFile.SelectNodes("//rdBookmarks/Bookmark[@Name='" + reportName + "']");

                        if (selectBookmarkToClear != null)
                        {

                            foreach (XmlNode bookmarkNode in selectBookmarkToClear)
                            {
                                XmlElement bookmarkElement = (XmlElement)bookmarkNode;

                                if (!bookmarkElement.IsEmpty)
                                {                                                 //blank
                                    bookmarkElement.SetAttribute("CustomColumn2", "");
                                    xmlBookmarkFile.Save(bookmarkFilePath);
                                }
                            }
                                 
                        }
                    }
                    catch(Exception ex)
                    {
                         System.Console.WriteLine(ex.Message);
                    }


                }

        }
    }
}



