﻿using System;
using System.Collections.Generic;
using System.Xml;

using rdPlugin;

namespace GaugeRepeats
{
    public class LogiPlugin
    {
        public void createGaugeCharts(rdServerObjects rdObjects)
        {
            // Get Report Defintion
            XmlDocument xDef = new XmlDocument();
            xDef.LoadXml(rdObjects.CurrentDefinition);

            rdObjects.AddDebugMessage("Gauge Chart Plugin", "Loaded Definition", "View Definition", xDef);

            // Get template chart
            XmlElement gaugeChart = (XmlElement) xDef.SelectSingleNode("//ElementTemplate[@ID='GaugeTemplate']/Gauge");

            String chartID = gaugeChart.GetAttribute("ID");

            rdObjects.AddDebugMessage("", "Chart Template", "View Chart", gaugeChart);

            // Get Data from Local element
            XmlDocument xData = new XmlDocument();
            String sCurrDataFile = rdObjects.CurrentDataFile;
            xData.Load(sCurrDataFile);

            rdObjects.AddDebugMessage("", "Data Loaded", "View Data", xData);

            XmlElement xBody = (XmlElement) xDef.SelectSingleNode("//Body");

            //loop through the dataset and create a chart from the template for each row.
            foreach (XmlElement dRow in xData.SelectSingleNode("//rdData").ChildNodes)
            {
                String sChartID = chartID + dRow.GetAttribute("seq");

                XmlElement gaugeClone = (XmlElement) gaugeChart.CloneNode(true);
                gaugeClone.SetAttribute("ID", sChartID);

                xBody.AppendChild(gaugeClone);
            }

            rdObjects.AddDebugMessage("", "Definition updated", "View Definition", xDef);

            // Return same dataset
            xData.Save(rdObjects.ReturnedDataFile);

            // Save the changes to the report
            rdObjects.CurrentDefinition = xDef.OuterXml;
        }

    }
}
