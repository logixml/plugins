﻿using System;
using System.Collections;
using rdPlugin;

namespace LinkVersioning
{
    public class LinkVersioning
    {
        public void ReviewHTML (rdServerObjects rdO)
        {
            Hashtable p = rdO.PluginParameters;

            String ver = p["version"].ToString();

            rdO.AddDebugMessage("", "Before", rdO.ResponseHtml);

            String html = rdO.ResponseHtml;

            html = html.Replace(".css\"", ".css?v" + ver + "\"");
            html = html.Replace(".css&amp;", ".css%3Fv" + ver + "&amp;");

            html = html.Replace(".js\"", ".js?v" + ver + "\"");

            rdO.ResponseHtml = html;

            rdO.AddDebugMessage("", "After", rdO.ResponseHtml);
            
        }
    }
}
