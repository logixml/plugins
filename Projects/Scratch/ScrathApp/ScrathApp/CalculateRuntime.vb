﻿Imports System.Xml

Module CalculateRuntime

    Sub Main()
        Dim sSchedule As String = "<Schedule Type=""Weekly"" StartDate=""2017-11-28"" FirstRunTime=""21:00"" RunEveryXWeeks=""1"" DaysOfWeek=""62"" />"
        Console.WriteLine(sCalculateNextRunTime(sSchedule, Nothing))

        Do Until (Console.ReadKey.Key = ConsoleKey.Escape)
            'Put your logic here.       
        Loop

    End Sub

    Function sCalculateNextRunTime(ByVal sScheduleXml As String, ByVal dtLastRun As DateTime) As DateTime
        ' Possible attributes of "Schedule" element are:
        ' Type - Frequency category of the schedule. It can be set to: Once, Daily, Weekly, Monthly, MonthlyDayOfWeek
        ' StartDate - Added for Ad Hoc - The date all scheduled activity happens ON or AFTER
        ' EndDate - Added for Ad Hoc - The date after which the schedule will no longer run (optional)
        ' FirstRunTime - time the schedule should run.
        '   (In my opinion this should simply be called RunTime, because "frist" or otherwise is determined by date) - 
        ' LastRunTime - (Not used by Ad Hoc)
        ' RunOnceAtTime - Used in the ETL product instead of Type=Once
        ' RunEveryXMinutes - (Not used by Ad Hoc)
        ' RunEveryXHours - (Not used by Ad Hoc)
        ' RunEveryXDays - Interval for Type=Daily
        ' RunEveryXWeeks - Interval for Type=Weekly (always 1 in Ad Hoc)
        ' RunEveryXMonths - Interval for Type=Monthly (Not used in Ad Hoc)
        ' DayOfWeek - Set to a system value of the day of week. (Ad Hoc only uses this one with Type=MonthlyDayOfWeek)
        ' DaysOfWeek - Added for Ad Hoc - Binary sum of days of week
        '   (see DaysOfTheWeek Enum) when the task should run Weekly on particular days of the week.
        ' Months - Added for Ad Hoc - Binary sum of months of the year selected to run the schedule.
        '   (See MonthsOfTheYear Enum) It is used when the task should run in particular months.
        ' DayOfMonth - Which day of the month (1-31) should the task run on? For Type=Monthly
        ' MonthlyNthOccurrence - Added for Ad Hoc and used in conjunction with Type=MonthlyDayOfWeek
        '   Which day of week e.g. "third Sunday of every month"! :)

        Dim iNowYear As Integer = Now.Year
        Dim iNowMonth As Integer = Now.Month
        Dim iNowDay As Integer = Now.Day
        Dim iNowHour As Integer = Now.Hour
        Dim iNowMinute As Integer = Now.Minute

        Dim dtNextRun As DateTime
        Dim dtStartDateTime As DateTime
        Dim dtEndDateTime As DateTime

        ' Load the XML Schedule definition into a document.
        Dim xmlScheduleDef As New XmlDocument()
        xmlScheduleDef.LoadXml("<rdSchedule>" & sScheduleXml & "</rdSchedule>")

        ' All we need is a date and time in the future, that the task needs to run.

        ' Grab the Schedule element and initialize the time offsets.
        Dim eleSchedule As XmlElement = xmlScheduleDef.DocumentElement.FirstChild
        Dim sType As String = eleSchedule.GetAttribute("Type")

        Dim iStartYear As Integer = iNowYear
        Dim iStartMonth As Integer = iNowMonth
        Dim iStartDay As Integer = iNowDay
        Dim iStartHour As Integer = iNowHour
        Dim iStartMinute As Integer = iNowMinute

        If eleSchedule.HasAttribute("StartDate") Then
            Dim d As Date = CType(eleSchedule.GetAttribute("StartDate"), Date)
            iStartYear = d.Year
            iStartMonth = d.Month
            iStartDay = d.Day
        End If

        ' If the user has specified a first run time or a specific run time, we need to use that.
        If eleSchedule.HasAttribute("FirstRunTime") Then
            Dim sRunTime As String = eleSchedule.GetAttribute("FirstRunTime")
            iStartHour = CInt(sRunTime.Substring(0, 2))
            iStartMinute = CInt(sRunTime.Substring(3, 2))
        ElseIf eleSchedule.HasAttribute("RunOnceAtTime") Then
            Dim sRunTime As String = eleSchedule.GetAttribute("RunOnceAtTime")
            iStartHour = CInt(sRunTime.Substring(0, 2))
            iStartMinute = CInt(sRunTime.Substring(3, 2))
        End If
        dtStartDateTime = New DateTime(iStartYear, iStartMonth, iStartDay, iStartHour, iStartMinute, 0)

        Dim bEndDateSet As Boolean = False
        Dim iEndYear As Integer = iNowYear
        Dim iEndMonth As Integer = iNowMonth
        Dim iEndDay As Integer = iNowDay
        Dim iEndHour As Integer = iNowHour
        Dim iEndMinute As Integer = iNowMinute

        ' See if we have an end date/time
        If eleSchedule.HasAttribute("EndDate") Then
            bEndDateSet = True
            Dim d As Date = CType(eleSchedule.GetAttribute("EndDate"), Date)
            iEndYear = d.Year
            iEndMonth = d.Month
            iEndDay = d.Day
        End If

        If eleSchedule.HasAttribute("LastRunTime") Then
            bEndDateSet = True
            Dim sRunTime As String = eleSchedule.GetAttribute("LastRunTime")
            iEndHour = CInt(sRunTime.Substring(0, 2))
            iEndMinute = CInt(sRunTime.Substring(3, 2))
        End If
        ' I do not know what is LastRunTime, but let's keep it for support old versions
        If eleSchedule.HasAttribute("EndTime") Then
            bEndDateSet = True
            Dim sEndTime As String = eleSchedule.GetAttribute("EndTime")
            iEndHour = CInt(sEndTime.Substring(0, 2))
            iEndMinute = CInt(sEndTime.Substring(3, 2))
        Else
            If bEndDateSet AndAlso (sType = "Daily" OrElse sType = "Weekly" OrElse sType = "Monthly") Then 'REPDEV-20295 Make sure the end date includes the entire day.
                iEndHour = "23"
                iEndMinute = "59"
            End If
        End If

        If bEndDateSet Then
            dtEndDateTime = New DateTime(iEndYear, iEndMonth, iEndDay, iEndHour, iEndMinute, 0)
        End If

        dtNextRun = dtLastRun
        If sType = "Once" Then '10446 Case 1
            ' This is the most straighforward case where we already have everything
            ' to determine the next run time
            'If bEndDateSet Then
            '    If dtStartDateTime >= Now And dtStartDateTime <= dtEndDateTime Then
            '        dtNextRun = dtStartDateTime
            '    End If
            'Else
            'If dtStartDateTime >= Now Then
            dtNextRun = dtStartDateTime
            'Else
            '    dtNextRun = Now.Date '10803
            'End If
            'End If
        Else
            ' RunEveryXMinutes and RunEveryXHours can be set on any of these types!
            Dim nMonthOffset As Integer = 0
            Dim nWeekOffset As Integer = 0
            Dim nDayOffset As Integer = 0
            'Dim nHourOffset As Integer = 0
            Dim nMinuteOffset As Integer = 0
            Dim nMinuteDuration As Integer = 0
            Dim dtRunTime As DateTime = dtLastRun

            If eleSchedule.HasAttribute("RunEveryXMinutes") Then
                nMinuteOffset = CInt(eleSchedule.GetAttribute("RunEveryXMinutes"))
            ElseIf eleSchedule.HasAttribute("RunEveryXHours") Then
                'nHourOffset = CInt(eleSchedule.GetAttribute("RunEveryXHours"))
                nMinuteOffset = 60 * CInt(eleSchedule.GetAttribute("RunEveryXHours"))
            End If

            If eleSchedule.HasAttribute("RunForXMinutes") Then
                nMinuteDuration = CInt(eleSchedule.GetAttribute("RunForXMinutes"))
            ElseIf eleSchedule.HasAttribute("RunForXHours") Then
                'nHourOffset = CInt(eleSchedule.GetAttribute("RunEveryXHours"))
                nMinuteDuration = 60 * CInt(eleSchedule.GetAttribute("RunForXHours"))
            End If

            ' If we are passed the end date, don't bother
            If bEndDateSet And dtEndDateTime <= Now Then
            Else
                Select Case sType
                    Case "Minutes", "Hourly" '10193
                        If nMinuteOffset = 0 Then
                            nMinuteOffset = 1
                        End If

                        If dtStartDateTime > Now Then
                            If bEndDateSet Then
                                If dtStartDateTime < dtEndDateTime Then
                                    dtRunTime = dtStartDateTime
                                End If
                            Else
                                dtRunTime = dtStartDateTime
                            End If
                        Else
                            ' This schedule started a while back and continues.
                            ' Calculate the first eligible date
                            Dim m As Integer = DateDiff(DateInterval.Minute, dtStartDateTime, Now)
                            'd = CInt(d / nDayOffset)
                            m = CInt(Math.Floor(m / nMinuteOffset)) 'Changed this to floor so that we will always get the most recent last start time less than now.

                            dtRunTime = dtStartDateTime.AddMinutes(m * nMinuteOffset)
                            'If dtRunTime.Date = Today Then
                            '' Today is an eligible date. 
                            If dtRunTime <= Now Then
                                'Keep on adding Interval minutes till we are past current time.
                                While dtRunTime < DateTime.Now
                                    dtRunTime = dtRunTime.AddMinutes(nMinuteOffset)
                                End While
                            End If
                        End If
                    Case "Daily"
                        ' First adjust the run date/time
                        If eleSchedule.HasAttribute("RunEveryXDays") Then
                            nDayOffset = CInt(eleSchedule.GetAttribute("RunEveryXDays"))
                        End If
                        If nDayOffset = 0 Then nDayOffset = 1

                        If dtStartDateTime > Now Then
                            If bEndDateSet Then
                                If dtStartDateTime < dtEndDateTime Then
                                    dtRunTime = dtStartDateTime
                                End If
                            Else
                                dtRunTime = dtStartDateTime
                            End If
                        Else
                            ' This schedule started a while back and continues.
                            ' Calculate the first eligible date
                            Dim d As Integer = DateDiff(DateInterval.Day, dtStartDateTime, Now)
                            'd = CInt(d / nDayOffset)
                            d = CInt(Math.Floor(d / nDayOffset)) 'Changed this to floor so that we will always get the most recent last start time less than now.

                            dtRunTime = dtStartDateTime.AddDays(d * nDayOffset)
                            'If dtRunTime.Date = Today Then
                            '' Today is an eligible date. 
                            If dtRunTime <= Now Then
                                If nMinuteOffset > 0 And nMinuteDuration > 0 Then
                                    'First check if we over the duration. In that case start next day.
                                    Dim dtEndDurationDateTime As Date = dtRunTime.AddMinutes(nMinuteDuration)
                                    If dtEndDurationDateTime > Now Then
                                        Dim dtOrig As Date = dtRunTime

                                        'Keep on adding Interval minutes till we are past current time.
                                        While dtRunTime < DateTime.Now
                                            dtRunTime = dtRunTime.AddMinutes(nMinuteOffset)
                                        End While

                                        If dtRunTime >= dtEndDurationDateTime Then
                                            'The next run time is after the end duration. Set to next day.
                                            dtRunTime = dtOrig
                                            dtRunTime = dtRunTime.AddDays(nDayOffset)
                                        End If

                                        'Make sure the next time does not overlap with the next start date time
                                        Dim dtNext As Date = dtOrig.AddDays(nDayOffset)
                                        If dtRunTime > dtNext Then
                                            dtRunTime = dtNext
                                        End If
                                    Else
                                        dtRunTime = dtRunTime.AddDays(nDayOffset)
                                    End If
                                Else
                                    dtRunTime = dtRunTime.AddDays(nDayOffset)
                                End If
                            Else
                                ' We have a good time already
                            End If
                            'Else
                            '    ' Minutely and hourly don't matter anymore, because we are on the wrong date.
                            '    dtRunTime = dtRunTime.AddDays(nDayOffset)
                            'End If
                        End If
                        '' Adjust start date/time so it's as close to today as possible
                        'If dtStartDateTime < dtLastRun Then
                        '    dtStartDateTime = New DateTime(dtLastRun.Year, dtLastRun.Month, dtLastRun.Day, dtStartDateTime.Hour, dtStartDateTime.Minute, 0)
                        'End If

                    Case "Weekly"
                        If eleSchedule.HasAttribute("RunEveryXWeeks") Then
                            nWeekOffset = CInt(eleSchedule.GetAttribute("RunEveryXWeeks"))
                        End If
                        If nWeekOffset = 0 Then nWeekOffset = 1

                        Dim nDayOfWeek As Integer = 0
                        If eleSchedule.HasAttribute("DaysOfWeek") Then
                            nDayOfWeek = CInt(eleSchedule.GetAttribute("DaysOfWeek"))
                        ElseIf eleSchedule.HasAttribute("DayOfWeek") Then
                            nDayOfWeek = CInt(eleSchedule.GetAttribute("DayOfWeek"))
                        End If
                        If nDayOfWeek = 0 Then nDayOfWeek = Today.DayOfWeek ' If not set, assume it to be today

                        'Dim stupid As New rdSchedulerCalcRunTime
                        'dtRunTime = stupid.GetWeeklyTime(nDayOfWeek, dtStartDateTime, nWeekOffset, iStartHour, iStartMinute)

                        If dtStartDateTime > Now Then
                            dtRunTime = dtStartDateTime

                            Do While (2 ^ CInt(dtRunTime.DayOfWeek) And CInt(nDayOfWeek)) <> (2 ^ CInt(dtRunTime.DayOfWeek))
                                dtRunTime = dtRunTime.AddDays(1)

                                If CInt(dtRunTime.DayOfWeek) = 0 Then
                                    ' We have gone through the week without finding an eligible day.
                                    ' Let's go back to the start of the week and add nWeekOffset
                                    dtRunTime = dtRunTime.AddDays(-7 + nWeekOffset * 7)
                                End If
                            Loop
                        Else
                            ' This schedule started a while back and continues.
                            ' Calculate the first eligible date
                            Dim dStartDateWeekStart As Date = dtStartDateTime.Date
                            Do While CInt(dStartDateWeekStart.DayOfWeek) <> 0
                                dStartDateWeekStart = dStartDateWeekStart.AddDays(-1)
                            Loop

                            Dim dNowDateWeekStart As DateTime = Now.Date
                            Do While CInt(dNowDateWeekStart.DayOfWeek) <> 0
                                dNowDateWeekStart = dNowDateWeekStart.AddDays(-1)
                            Loop

                            Dim d As Integer = DateDiff(DateInterval.Day, dStartDateWeekStart.Date, dNowDateWeekStart.Date)
                            'd = CInt(d \ (nWeekOffset * 7))
                            d = CInt(Math.Floor(d \ (nWeekOffset * 7)))
                            If d >= nWeekOffset Then
                                dStartDateWeekStart = dStartDateWeekStart.AddDays(d * nWeekOffset * 7)
                            End If

                            If nMinuteOffset > 0 And nMinuteDuration > 0 Then
                                'This code is different for Minute Intervals
                                dtRunTime = New DateTime(dStartDateWeekStart.Year, dStartDateWeekStart.Month, dStartDateWeekStart.Day, iStartHour, iStartMinute, 0)

                                'Find the first eligible day for this week.
                                Do While (2 ^ CInt(dtRunTime.DayOfWeek) And CInt(nDayOfWeek)) <> (2 ^ CInt(dtRunTime.DayOfWeek))
                                    dtRunTime = dtRunTime.AddDays(1)

                                    If CInt(dtRunTime.DayOfWeek) = 0 Then
                                        ' We have gone through the week without finding an eligible day.
                                        ' Let's go back to the start of the week and add nWeekOffset
                                        dtRunTime = dtRunTime.AddDays(-7 + nWeekOffset * 7)
                                    End If
                                Loop

                                'Now dtRunTime is a valid day and date so we check for end date
                                Dim dtEndDurationTime As Date
                                Dim dtNext As Date
                                Do While dtRunTime <= Now

                                    'Get the next available day
                                    dtNext = dtRunTime.AddDays(1)

                                    If CInt(dtNext.DayOfWeek) = 0 Then
                                        ' We have gone through the week without finding an eligible day.
                                        ' Let's go back to the start of the week and add nWeekOffset
                                        dtNext = dtNext.AddDays(-7 + nWeekOffset * 7)
                                    End If

                                    Do While (2 ^ CInt(dtNext.DayOfWeek) And CInt(nDayOfWeek)) <> (2 ^ CInt(dtNext.DayOfWeek))
                                        dtNext = dtNext.AddDays(1)

                                        If CInt(dtNext.DayOfWeek) = 0 Then
                                            ' We have gone through the week without finding an eligible day.
                                            ' Let's go back to the start of the week and add nWeekOffset
                                            dtNext = dtNext.AddDays(-7 + nWeekOffset * 7)
                                        End If
                                    Loop

                                    'Check if the end time if greater than Now
                                    dtEndDurationTime = dtRunTime.AddMinutes(nMinuteDuration)
                                    If dtEndDurationTime > Now Then
                                        'find the next available time based on intervals.
                                        While dtRunTime < DateTime.Now
                                            dtRunTime = dtRunTime.AddMinutes(nMinuteOffset)
                                        End While

                                        If dtRunTime >= dtEndDurationTime Then
                                            'The next run time is after the end duration. Set to next day.
                                            dtRunTime = dtNext
                                        End If

                                        'check if we are not overlapping with the next start time
                                        If dtRunTime > dtNext Then
                                            dtRunTime = dtNext
                                        End If

                                    Else
                                        'End Time is in past. Move to next available date
                                        dtRunTime = dtNext
                                    End If
                                Loop

                            Else
                                ' Now we are at the beginning of the right period. If it's more than a week ago
                                ' we have to move to the next eligible week.
                                If dStartDateWeekStart < dNowDateWeekStart Then
                                    dStartDateWeekStart = dStartDateWeekStart.AddDays(nWeekOffset * 7)
                                End If
                                dtRunTime = New DateTime(dStartDateWeekStart.Year, dStartDateWeekStart.Month, dStartDateWeekStart.Day, iStartHour, iStartMinute, 0)

                                Do While (2 ^ CInt(dtRunTime.DayOfWeek) And CInt(nDayOfWeek)) <> (2 ^ CInt(dtRunTime.DayOfWeek))
                                    dtRunTime = dtRunTime.AddDays(1)

                                    If CInt(dtRunTime.DayOfWeek) = 0 Then
                                        ' We have gone through the week without finding an eligible day.
                                        ' Let's go back to the start of the week and add nWeekOffset
                                        dtRunTime = dtRunTime.AddDays(-7 + nWeekOffset * 7)
                                    End If
                                Loop

                                ' Let's make sure the date/time is in the future
                                Do While dtRunTime <= Now
                                    dtRunTime = dtRunTime.AddDays(1)

                                    If CInt(dtRunTime.DayOfWeek) = 0 Then
                                        ' We have gone through the week without finding an eligible day.
                                        ' Let's go back to the start of the week and add nWeekOffset
                                        dtRunTime = dtRunTime.AddDays(-7 + nWeekOffset * 7)
                                    End If

                                    Do While (2 ^ CInt(dtRunTime.DayOfWeek) And CInt(nDayOfWeek)) <> (2 ^ CInt(dtRunTime.DayOfWeek))
                                        dtRunTime = dtRunTime.AddDays(1)

                                        If CInt(dtRunTime.DayOfWeek) = 0 Then
                                            ' We have gone through the week without finding an eligible day.
                                            ' Let's go back to the start of the week and add nWeekOffset
                                            dtRunTime = dtRunTime.AddDays(-7 + nWeekOffset * 7)
                                        End If
                                    Loop
                                Loop
                            End If
                        End If

                    Case "Monthly"
                        Dim nDayOfMonth As Integer
                        Dim iYear As Integer = iStartYear
                        Dim iMonth As Integer = iStartMonth
                        Dim iHour As Integer = iStartHour
                        Dim iMinute As Integer = iStartMinute

                        If eleSchedule.HasAttribute("DayOfMonth") Then
                            nDayOfMonth = CInt(eleSchedule.GetAttribute("DayOfMonth"))
                        Else
                            nDayOfMonth = iStartDay
                        End If
                        Dim iDay As Integer = nDayOfMonth

                        If eleSchedule.HasAttribute("RunEveryXMonths") Then
                            nMonthOffset = CInt(eleSchedule.GetAttribute("RunEveryXMonths"))

                            dtRunTime = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)

                            ' If this is in the past, find the next eligible date
                            If dtRunTime <= Now Then
                                Dim m As Integer = DateDiff(DateInterval.Month, dtRunTime.Date, Now.Date)
                                'm = CInt(m / nMonthOffset)
                                m = CInt(Math.Floor(m / nMonthOffset))

                                dtRunTime = dtRunTime.AddMonths(m * nMonthOffset)
                                ' If this is still in the past, add one more interval
                                Dim dtNext As Date
                                If dtRunTime <= Now Then
                                    'First find the next eligible date so that we don't go beyond that.
                                    dtNext = dtRunTime.AddMonths(nMonthOffset)

                                    If nMinuteOffset > 0 And nMinuteDuration > 0 Then
                                        Dim dtEndDurationDateTime As Date = dtRunTime.AddMinutes(nMinuteDuration)
                                        If dtEndDurationDateTime > Now Then
                                            'Keep on adding Interval minutes till we are past current time.
                                            While dtRunTime < DateTime.Now
                                                dtRunTime = dtRunTime.AddMinutes(nMinuteOffset)
                                            End While

                                            If dtRunTime >= dtEndDurationDateTime Then
                                                'The next run time is after the end duration. Set to next day.
                                                dtRunTime = dtNext
                                            End If

                                            'Make sure the next time does not overlap with the next start date time
                                            If dtRunTime > dtNext Then
                                                dtRunTime = dtNext
                                            End If
                                        Else
                                            dtRunTime = dtNext
                                        End If
                                    Else
                                        dtRunTime = dtNext
                                    End If
                                End If
                            End If

                        ElseIf eleSchedule.HasAttribute("Months") Then
                            Dim nMonths As Integer = CInt(eleSchedule.GetAttribute("Months"))

                            If dtStartDateTime <= Now Then
                                iYear = iNowYear
                                iMonth = iNowMonth
                            End If

                            Do While (2 ^ (iMonth - 1) And nMonths) <> (2 ^ (iMonth - 1))
                                iMonth += 1
                                If iMonth > 12 Then
                                    iMonth = 1
                                    iYear += 1
                                End If
                            Loop

                            ' This is the closest eligible date to today
                            dtRunTime = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)

                            ' If this is in the past, go to the next one
                            If dtRunTime < Now Then
                                iMonth += 1
                                If iMonth > 12 Then
                                    iMonth = 1
                                    iYear += 1
                                End If

                                Do While (2 ^ (iMonth - 1) And nMonths) <> (2 ^ (iMonth - 1))
                                    iMonth += 1
                                    If iMonth > 12 Then
                                        iMonth = 1
                                        iYear += 1 '10803
                                    End If
                                Loop
                                Dim dtNext As Date = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)

                                'NOw we check for the interval
                                If nMinuteOffset > 0 And nMinuteDuration > 0 Then
                                    Dim dtEndDurationDateTime As Date = dtRunTime.AddMinutes(nMinuteDuration)
                                    If dtEndDurationDateTime > Now Then
                                        'Keep on adding Interval minutes till we are past current time.
                                        While dtRunTime < DateTime.Now
                                            dtRunTime = dtRunTime.AddMinutes(nMinuteOffset)
                                        End While

                                        If dtRunTime >= dtEndDurationDateTime Then
                                            'The next run time is after the end duration. Set to next day.
                                            dtRunTime = dtNext
                                        End If

                                        'Make sure the next time does not overlap with the next start date time
                                        If dtRunTime > dtNext Then
                                            dtRunTime = dtNext
                                        End If
                                    Else
                                        dtRunTime = dtNext
                                    End If
                                Else
                                    dtRunTime = dtNext
                                End If
                            End If
                        End If
                    Case "MonthlyDayOfWeek"
                        If eleSchedule.HasAttribute("RunEveryXMonths") Then
                            nMonthOffset = CInt(eleSchedule.GetAttribute("RunEveryXMonths"))
                            ' TODO: code for this!
                        ElseIf eleSchedule.HasAttribute("Months") Then
                            Dim nMonths As Integer = CInt(eleSchedule.GetAttribute("Months"))
                            Dim nDayOfWeek As Integer = 0
                            Dim MonthlyNthOccurrence As Integer = 1

                            'If eleSchedule.HasAttribute("DayOfWeek") Then
                            '    nDayOfWeek = CInt(eleSchedule.GetAttribute("DayOfWeek"))
                            'ElseIf eleSchedule.HasAttribute("DaysOfWeek") Then
                            '    nDayOfWeek = CInt(eleSchedule.GetAttribute("DaysOfWeek"))
                            'End If
                            If eleSchedule.HasAttribute("MonthlyDayOfWeek") Then '10921
                                nDayOfWeek = CInt(eleSchedule.GetAttribute("MonthlyDayOfWeek"))
                            End If
                            If nDayOfWeek = 0 Then nDayOfWeek = Today.DayOfWeek ' If not set, assume it to be today

                            If eleSchedule.HasAttribute("MonthlyNthOccurrence") Then
                                MonthlyNthOccurrence = CInt(eleSchedule.GetAttribute("MonthlyNthOccurrence")) + 1 '10921
                            End If

                            Dim iYear As Integer = iNowYear
                            Dim iMonth As Integer = iNowMonth
                            Dim iDay As Integer = 1
                            Dim iHour As Integer = iStartHour
                            Dim iMinute As Integer = iStartMinute

                            If dtStartDateTime > Now Then
                                iYear = iStartYear
                                iMonth = iStartMonth
                            ElseIf dtLastRun > dtStartDateTime Then
                                iYear = dtLastRun.Year
                                iMonth = dtLastRun.Month
                            End If

                            Do While (2 ^ (iMonth - 1) And nMonths) <> (2 ^ (iMonth - 1))
                                iMonth += 1
                                If iMonth > 12 Then
                                    iMonth = 1
                                    iYear += 1
                                End If
                            Loop

                            If MonthlyNthOccurrence = 5 Then
                                ' Last
                                ' Since we don't know what the last day of the month is, go to
                                ' the first day of the next month and count backwards.
                                iMonth += 1
                                If iMonth > 12 Then
                                    iMonth = 1
                                    iYear += 1
                                End If
                                dtRunTime = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)
                                dtRunTime = dtRunTime.AddDays(-1)

                                Do While nDayOfWeek <> dtRunTime.DayOfWeek
                                    dtRunTime = dtRunTime.AddDays(-1)
                                Loop
                            Else
                                ' Start from first day of the month and see if it's the right day of week.
                                ' Go to the right day of week and add as many weeks as necessary.
                                dtRunTime = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)

                                Do While nDayOfWeek <> dtRunTime.DayOfWeek
                                    dtRunTime = dtRunTime.AddDays(1)
                                Loop

                                If MonthlyNthOccurrence > 1 Then
                                    dtRunTime = dtRunTime.AddDays(7 * (MonthlyNthOccurrence - 1))
                                End If
                            End If

                            ' We have arrived at the desired day of the week of the first eligible month
                            ' See if this date is in the future.
                            If dtRunTime < Now Then
                                iYear = dtRunTime.Year
                                iMonth = dtRunTime.Month
                                iDay = 1

                                iMonth += 1
                                If iMonth > 12 Then
                                    iMonth = 1
                                    iYear += 1
                                End If

                                Do While (2 ^ (iMonth - 1) And nMonths) <> (2 ^ (iMonth - 1))
                                    iMonth += 1
                                    If iMonth > 12 Then
                                        iMonth = 1
                                        iYear += 1 '10921
                                    End If
                                Loop

                                ' Start over with finding the right day of the week!
                                Dim dtNext As Date
                                iDay = 1
                                If MonthlyNthOccurrence = 5 Then
                                    ' Last
                                    ' Since we don't know what the last day of the month is, go to
                                    ' the first day of the next month and count backwards.
                                    iMonth += 1
                                    If iMonth > 12 Then
                                        iMonth = 1
                                        iYear += 1
                                    End If
                                    dtNext = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)
                                    dtNext = dtNext.AddDays(-1)

                                    Do While nDayOfWeek <> dtNext.DayOfWeek
                                        dtNext = dtNext.AddDays(-1)
                                    Loop
                                Else
                                    ' Start from first day of the month and see if it's the right day of week.
                                    ' Go to the right day of week and add as many weeks as necessary.
                                    dtNext = New DateTime(iYear, iMonth, iDay, iHour, iMinute, 0)

                                    Do While nDayOfWeek <> dtNext.DayOfWeek
                                        dtNext = dtNext.AddDays(1)
                                    Loop

                                    If MonthlyNthOccurrence > 1 Then
                                        dtNext = dtNext.AddDays(7 * (MonthlyNthOccurrence - 1))
                                    End If
                                End If

                                If nMinuteOffset > 0 And nMinuteDuration > 0 Then
                                    Dim dtEndDurationDateTime As Date = dtRunTime.AddMinutes(nMinuteDuration)
                                    If dtEndDurationDateTime > Now Then
                                        'Keep on adding Interval minutes till we are past current time.
                                        While dtRunTime < DateTime.Now
                                            dtRunTime = dtRunTime.AddMinutes(nMinuteOffset)
                                        End While

                                        If dtRunTime >= dtEndDurationDateTime Then
                                            'The next run time is after the end duration. Set to next day.
                                            dtRunTime = dtNext
                                        End If

                                        'Make sure the next time does not overlap with the next start date time
                                        If dtRunTime > dtNext Then
                                            dtRunTime = dtNext
                                        End If
                                    Else
                                        dtRunTime = dtNext
                                    End If
                                Else
                                    dtRunTime = dtNext
                                End If

                            End If
                        End If
                End Select
            End If

            ' Check if the date that we came up with is past the end date
            If bEndDateSet And dtRunTime > dtEndDateTime Then
                dtRunTime = dtLastRun
            End If

            dtNextRun = dtRunTime
        End If

        Return dtNextRun
    End Function

End Module
