# Collection of Plugin Samples

This includes all plugin projects held by Joe Viscome for Info under .Net and Java.

### What is this repository for?

- Use this repsitory as a resource for some example plugins and examples that can be provided to customers
- Not all content is generally useful, so some key plugins have been called out below

### Key Sample Plugins

#### Angular

- This application was built in conjunction with REPDEV-28218 (Zendesk #38132)
  - It provides a basic embedding application with Angular2 to show potential issues in the EmbedJS architecture and Single Page Applications (SPAs)

#### Java

- Java/Java/Avetta_Colors
  - Used by Avetta to set the Color for Series in the Analysis Grid's Analysis Chart based on a column in the data
  - Java/Java/Avetta_Plugins is a different version of the Avetta_Colors implementation
- Java/Java/DL_Plugin
  - This provides examples of how to work with the data in a Datalayer.Plugin
  - The first method, runDLPluginAsFile, will pass the data as a file. The second method, rdDLPluginAsDoc, will store the data as a XMLDocument and pass the object to the setCurrentData method
  - This was often used as an example to provide to customers that had trouble getting their data through the Datalayer.Plugin
- Java/JavaPlugin_Requests
  - Extract all Name/Value pairs from the Info Java request object and print them to the debug page
  - Used as a troubleshooting tool to determine what parameters were available and if any were missing

#### .NET

- Projects/DLPlugin_Schema
  - The .Net version of Java/Java/DL_Plugin above
- Projects/EOD/LogiPlugin_AlterPivotData
  - Used to strip the Crosstab naming convention from a datalayer so the underlying data could be used in a standard DataTable
- Projects/EOD/Synapsify and Projects/EOD/Synapsify.ReportToPNG
  - This plugin uses the PhantomJS instance in the engine to create a PNG image of the HTML that can be used as part of an export, or stored separately
  - Synapsify includes the sample definitions and DLL, Synapsify.ReportToPNG includes source of the plugin
- Projects/GeneratedSQLPlugin
  - Simple example of how to use the Generated SQL Plugin element
- Projects/LinkVersioning
  - Used to apply a version tag to all include files (JS, CSS) in the finished HTML of the report
- Projects/LogiPlugin/CountAttempts
  - Used to identify how main times a Settings file plugin executed
- Projects/MemLeak-ASPNET
  - Web Service (ASMX) generated to test memory leaks associated with Datalayer.WebService
- Projects/SignedPluginSample
  - Utilizing the rdPluginSigned.dll of the Info engine to test execution
- Projects/ZenPsyche/ZenPsyche
  - Generic REST service created to call Zendesk and extra data for use in ZenPsyche
- Projects/ZenPsyche/ZenPsycheBusinessTime
  - Methods to evaluate cases based on current Business Time, excluding weekends
